import styled from 'styled-components/native'
import colors from './colors'

const sizes = {
  x0: 11,
  x1: 13,
  x2: 15,
  x3: 17,
  x4: 19,
  x5: 21,
  x6: 24,
}

const defaultText = styled.Text`
  letter-spacing: 0;
  color: ${colors.darker};
  font-family: 'OpenSans-Regular';
`

const size_6x = styled(defaultText)`
  line-height: ${sizes.x5 * 1.5};
  font-size: ${sizes.x6};
`

const size_5x = styled(defaultText)`
  line-height: ${sizes.x5 * 1.5};
  font-size: ${sizes.x5};
`

const size_4x = styled(defaultText)`
  line-height: ${sizes.x4 * 1.5};
  font-size: ${sizes.x4};
`

const size_3x = styled(defaultText)`
  line-height: ${sizes.x3 * 1.5};
  font-size: ${sizes.x3};
`

const size_2x = styled(defaultText)`
  line-height: ${sizes.x2 * 1.5};
  font-size: ${sizes.x2};
`

const size_1x = styled(defaultText)`
  line-height: ${sizes.x1 * 1.5};
  font-size: ${sizes.x1};
`

const size_0x = styled(defaultText)`
  line-height: ${sizes.x0 * 1.5};
  font-size: ${sizes.x0};
`

const fonts = {
  size_0x,
  size_1x,
  size_2x,
  size_3x,
  size_4x,
  size_5x,
  size_6x,
}

export default fonts
