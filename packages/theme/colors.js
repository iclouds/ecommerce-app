export default {
  white: '#ffffff',
  light: '#F5F5F5', // Input
  dark: '#404040', // Botão secundario
  darker: '#2A2A2A', // Botões e Titulos
  green: '#4BB543',
  grey: '#919191', // Texto e Inputs(placeholders textos)
  greyDark: '#818181', // Texto e Inputs(placeholders textos)
  greyest: '#E1E1E1', // Cards(destaque)

  success: '#9AEC94',
  danger: '#F27878',
  warning: '#EFC900',

  transparent: 'transparent',
  orange: '#EE8E4E',
  brown: '#a2723c'
}
