import { Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

export default {
  baseRadius: 7,

  baseMargin_1x: 8,
  baseMargin_2x: 16,
  baseMargin_3x: 24,

  baseContainer: 12,

  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
}
