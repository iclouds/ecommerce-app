function RemoveSpecialCharacters(text) {
  const filteredText = text.replace(/\D/gm, '')

  return filteredText
}

export default RemoveSpecialCharacters
