function MaskValue(value) {
  if (value.length <= 3) return `R$ ${value},00`;

  return `R$ ${value}`;
}

function ConvertFloatToMoney(value) {
  const convertedValue = value
    .replace(".", ",")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".");

  return MaskValue(convertedValue);
}

export default ConvertFloatToMoney;
