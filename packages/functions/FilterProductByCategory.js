function MatchCategories(categories, pageCategory) {
  const matchCategories = categories.filter((category) => {
    if (category.name === pageCategory) return category;
  });

  return matchCategories
}

function FilterProductByCategory(allProducts, pageCategory) {
  const matchProducts = allProducts.filter((product) => {
    const matchCategories = MatchCategories(product.categories, pageCategory);

    if (matchCategories.length !== 0) return product;
  });

  return matchProducts;
}

export default FilterProductByCategory
