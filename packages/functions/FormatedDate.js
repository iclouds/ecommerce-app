function FormatedDate() {
  const day = new Date().getDate()
  const month = new Date().getMonth()
  const year = new Date().getFullYear()

  return `${day}-${month}-${year - 16}`
}

export default FormatedDate
