function FilterSpecificProduct(products, productId) {
  const productFiltered = products && products.filter(product => {
    if(product.id !== productId) return false

    return product
  })

  return productFiltered[0]
}

export default FilterSpecificProduct
