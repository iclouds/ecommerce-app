import styled from 'styled-components/native'
import { Platform } from 'react-native'

import { metrics } from 'theme'

export const Container = styled.ScrollView`
  padding: ${metrics.baseContainer}px;
  margin-bottom: ${Platform.OS !== 'android' ? metrics.baseMargin_2x : 0}px;
`

export const Products = styled.View`
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: space-between;

  padding-bottom: ${metrics.baseContainer}px;
`

export const ProductList = styled.FlatList`
  padding: ${metrics.baseContainer}px;
  margin-bottom: ${Platform.OS !== 'android' ? metrics.baseMargin_2x : 0}px;
`

export const Button = styled.TouchableOpacity``
