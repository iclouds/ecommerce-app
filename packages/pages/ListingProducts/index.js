import React from 'react'
import { withNavigation } from 'react-navigation'

import { ProductCard, ProductCardPlaceholder } from 'components'

import { Container, Products, ProductList } from './styles'

function ListingProducts({
  navigation,
  products,
  isLoaded,
  fetchMoreProducts,
  style,
}) {
  function renderProductCard({ id, name, price, sale_price, images }) {
    return (
      <ProductCard
        key={id}
        onPress={() =>
          navigation.navigate('Product', {
            productId: id,
            productName: name,
          })
        }
        name={name}
        price={price}
        sale_price={sale_price}
        image={images[0] ? images[0].src : false}
      />
    )
  }

  function renderProducts() {
    return (
      <ProductList
        style={style}
        data={products}
        numColumns={2}
        keyExtractor={item => String(item.id)}
        renderItem={({ item }) => renderProductCard(item)}
        columnWrapperStyle={{
          justifyContent: 'space-between',
          paddingBottom: 20,
        }}
        onEndReachedThreshold={0.01}
        onEndReached={() => fetchMoreProducts()}
      />
    )
  }

  function renderProductsPlaceholder() {
    return (
      <Container style={style}>
        <Products>
          <ProductCardPlaceholder />
          <ProductCardPlaceholder />
          <ProductCardPlaceholder />
          <ProductCardPlaceholder />
        </Products>
      </Container>
    )
  }

  return <>{isLoaded ? renderProducts() : renderProductsPlaceholder()}</>
}

ListingProducts.defaultProps = {
  fetchMoreProducts: () => {},
}

ListingProducts.defaultProps = {
  fetchMoreProducts: () => {},
}

export default withNavigation(ListingProducts)
