import React, { useState, useEffect } from "react";
import { Image, Dimensions } from "react-native";
import { withNavigation } from "react-navigation";

import { metrics } from "theme";
import { woocommerceApi } from 'services';
import { ConvertFloatToMoney } from "functions";
import { Toast, Select, Divider, ActionFooter } from "components";

import {
  HTML,
  Wrap,
  Arrow,
  Title,
  Price,
  Carousel,
  MoreInfo,
  Container,
  Description,
  Informations,
  SelectContainer,
  FeaturedCarousel,
  ExpandInformations
} from "./styles";

const { width } = Dimensions.get("window");

function ProductDetail({
  product,
  apiKeys,
  addToCart,
  editProduct,
  navigation,
  colors,
  isLoaded = false,
  cart
}) {

  const [isLoading, setIsLoading] = useState(true)
  const [productSizeAttributes, setProductSizeAttributes] = useState([]);
  const [productColorAttributes, setProductColorAttributes] = useState(['Padrão'])
  const [isAbleToAddToCart, setIsAbleToAddToCart] = useState(false);

  const [isInfoExpanded, setIsInfoExpanded] = useState(false);
  const [selectedAttributes, setSelectedAttributes] = useState({
    size: productSizeAttributes[0],
    color: productColorAttributes[0],
    amount: 1
  });

  const [productVariations, setProductVariations] = useState(false)

  const [productAttributesToCart, setProductAttributesToCart] = useState({
    id: product.id,
    name: product.name,
    images: product.images,
    price: product.price,
    shipping_class_id: product.shipping_class_id,
    dimensions: {
      ...product.dimensions,
      ...product.weight
    },
    ...selectedAttributes
  });
  const [showNotification, setShowNotification] = useState(false);

  useEffect(() => {
    getProductVariations();
  }, [])

  useEffect(() => {
    handleProductStock();

    getProductAttributes(["Numeração", "Tamanho"])
  }, [productVariations, cart])


  useEffect(() => {
    handleProductId();
  }, [selectedAttributes])

  async function getProductVariations(){
    const response = await woocommerceApi.getAllProductVariations(
      apiKeys.username,
      apiKeys.password,
      apiKeys.urlSdk,
      product.id,
    );

    setProductVariations(response);
  }

  function handleProductStock(){
    if(String(product.type) !== 'variable') {
      setIsAbleToAddToCart(Number(product.stock_quantity) > 0)
    }
  }

  function getProductAttributes(attribute) {
    const attributes = product.attributes.filter(productAttribute => {
      if (
        productAttribute.name === attribute[0] ||
        productAttribute.name === attribute[1]
      )
        return productAttribute;
    });

    return handleVariationsInStock(attributes)
  }

  function handleVariationsInStock(attributes) {
    if (product.type === `simple`){
      setIsLoading(false);
      return ['Padrão'];
    }

    if (!attributes.length > 0 || !productVariations.length > 0 ) {
      return []
    }

    let options = attributes[0].options;
    let optionsInStock = [];

    productVariations.map( ({ id, attributes, stock_quantity } ) => {
      const hasProductsToAddToCart = cart.data.filter(item => {
        return item.id === id && item.amount >= stock_quantity
      }).length === 0;

      const inStock = options.includes(attributes[0]['option']) && stock_quantity > 0 && hasProductsToAddToCart;

      if(inStock) optionsInStock.push(options.find(option => option === attributes[0]['option']))
    })

    setProductSizeAttributes(optionsInStock)
    setSelectedAttributes({
      ...selectedAttributes,
      ...optionsInStock
    })

    setIsAbleToAddToCart(optionsInStock.length > 0)
    setIsLoading(false);

    return optionsInStock;
  }

  function getSelectedAttribute(value) {
    setSelectedAttributes({ ...selectedAttributes, ...value });
  }

  function getToastNotification(type) {
    setShowNotification(type);
  }

  function handleAddToCart() {
    addToCart({
      ...productAttributesToCart,
      cartId: Math.random()
    });

    editProduct({
      ...productAttributesToCart,
      type: "subtraction"
    });

    navigation.navigate("Cart");
  }

  function handleProductId(){
    if(product.type !== "variable"){
      setIsLoading(false)
      return;
    }

    if(!productVariations.length > 0){
      return;
    }

    productVariations.map(variation => {
      if(['Tamanho', 'Numeração'].includes(variation.attributes[0]['name'])){

        const size = variation.attributes[0]['option'];

        if(String(size) === String(selectedAttributes.size)){
          setProductAttributesToCart({
            ...productAttributesToCart,
            parent_id: productAttributesToCart.id,
            id: variation.id,
            size
          })
        }
      }
    })
  }

  return (
    <>
      <Container>
        <Carousel>
          <FeaturedCarousel
            items={product.images}
            isLoaded={isLoaded}
            showModalOnPress={true}
          />
        </Carousel>

        <Wrap>
          <Title>{product.name}</Title>
          <Price>{ConvertFloatToMoney(product.price)}</Price>
        </Wrap>

        {product.attributes.length > 0 && (
          <SelectContainer>
            { productSizeAttributes.length > 0 &&
              <Select
                width="48"
                name={
                  (product.attributes[0] && product.attributes[0].name) ||
                  "Tamanho"
                }
                colors={colors}
                items={productSizeAttributes}
                selectItemToCart={getSelectedAttribute}
              />
            }

            {/* <Select
              width="48"
              name="Cor"
              colors={colors}
              items={productColorAttributes}
              selectItemToCart={getSelectedAttribute}
            /> */}
          </SelectContainer>
        )}

        <Divider
          styles={{
            marginBottom: metrics.baseMargin_2x,
            paddingHorizontal: metrics.baseContainer + 8
          }}
        />

        <Informations>
          <HTML
            html={product.description}
            renderers={
              {
                // p: ({ children }) => <Description>{children}</Description>,
                // img: ({ src }) => (
                //   <Image
                //     source={{ uri: src }}
                //     style={{ height: 400, width: 1000, resizeMode: "contain" }}
                //   />
                // )
              }
            }
          />
        </Informations>

        {/* <ExpandInformations onPress={() => setIsInfoExpanded(!isInfoExpanded)}>
          {product.description.length >= 250 && !isInfoExpanded && (
            <>
              <MoreInfo haveExpandOption={false}>mais informações</MoreInfo>
              <Arrow name="chevron-down" haveExpandOption={false} />
            </>
          )}

          {product.description.length >= 250 && isInfoExpanded && (
            <>
              <Arrow name="chevron-up" haveExpandOption={true} />
              <MoreInfo haveExpandOption={true}>ocultar informações</MoreInfo>
            </>
          )}
        </ExpandInformations> */}
      </Container>

      <ActionFooter
        cart
        input
        disabled={!isAbleToAddToCart}
        isLoading={isLoading}
        showToast={getToastNotification}
        selectItemToCart={getSelectedAttribute}
        stockProductsAmount={product.stock_quantity}
        primaryActionOnPress={() => handleAddToCart()}
        primaryText={
          isAbleToAddToCart
            ? "ADICIONAR AO CARRINHO"
            : "PRODUTO INDISPONÍVEL"
        }
      />

      {showNotification === "warn" && (
        <Toast
          textColor={colors.white}
          backgroundColor={colors.warning}
          text={`${product.stock_quantity} produtos disponíveis`}
        />
      )}

      {showNotification === "success" && (
        <Toast
          textColor={colors.light}
          backgroundColor={colors.green}
          text={`Adicionado com sucesso!`}
        />
      )}
    </>
  );
}

export default withNavigation(ProductDetail);
