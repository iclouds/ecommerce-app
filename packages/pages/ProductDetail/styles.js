import styled, { css } from "styled-components/native";
import { Dimensions, Image } from "react-native";
import HtmlRender from "react-native-render-html";
import Icon from "react-native-vector-icons/Feather";

import { metrics, fonts, colors } from "theme";
import { FeaturedCarousel as FeaturedCarouselStyled } from "components";

export const Container = styled.ScrollView`
  flex: 1;
`;

export const Carousel = styled.View`
  margin-left: 0;
`;

export const Informations = styled.View`
  padding: 0 8px 100px;

  align-items: center;
  justify-content: center;
`;

export const HTML = styled(HtmlRender).attrs({
  imagesMaxWidth: Dimensions.get("window").width * 0.9,
  ignoredStyles: ["width", "height", "float", "max-width", "display"],

  tagsStyles: {
    p: {
      marginBottom: 20,

      alignItems: "center",
      justifyContent: "center",

      fontSize: 13,
      letterSpacing: 0,
      color: colors.darker,
      lineHeight: 13 * 1.5,
      fontFamily: "OpenSans-Regular"
    },
    img: {
      resizeMode: "contain"
    }
  }
})``;

export const SelectContainer = styled.View`
  padding: 0 ${metrics.baseContainer + 5}px;
  margin-bottom: ${metrics.baseMargin_2x}px;

  flex-direction: row;
  justify-content: space-between;
`;

export const Description = styled(fonts.size_1x).attrs({
  ellipsizeMode: "tail"
})`
  margin-bottom: ${metrics.baseMargin_2x}px;
  padding-horizontal: ${metrics.baseContainer + 8}px;
  letter-spacing: -0.29px;

  color: #333;

  ${props =>
    !props.haveExpandOption &&
    css`
      margin-bottom: ${metrics.baseMargin_3x}px;
    `}
`;

export const ExpandInformations = styled.TouchableOpacity.attrs({
  activeOpacity: 0.8,
  hitSlop: {
    top: 30,
    right: 30,
    bottom: 30,
    left: 30
  }
})`
  align-items: center;
  justify-content: center;
  margin-bottom: ${metrics.baseContainer * 4}px;
`;

export const MoreInfo = styled(fonts.size_1x)`
  color: ${props => props.theme.secondary};

  ${props =>
    props.haveExpandOption &&
    css`
      margin-bottom: ${metrics.baseMargin_2x}px;
    `}
`;

export const Arrow = styled(Icon)`
  margin-top: -6px;
  margin-bottom: -6px;

  color: ${props => props.theme.secondary};

  font-size: 19px;
  font-weight: 600;

  ${props =>
    !props.haveExpandOption &&
    css`
      margin-bottom: ${metrics.baseMargin_2x}px;
    `}
`;

export const FeaturedCarousel = styled(FeaturedCarouselStyled)`
  padding-bottom: 20px;

  background: red;
`;

export const Title = styled(fonts.size_2x)`
  color: #333;
  letter-spacing: -0.33px;
`;

export const Price = styled(fonts.size_3x)`
  color: #333;
  font-weight: 600;
  letter-spacing: -0.38px;
`;

export const Wrap = styled.View`
  margin: ${metrics.baseContainer + 2}px ${metrics.baseContainer + 8}px;
`;
