import React, { useRef, useEffect } from "react";
import { View, Keyboard, Platform } from "react-native";

import { metrics } from "theme";
import { Input } from "components";

import { Row, ErrorMessage } from "../styles";

export default function Profile({ props, datepicker, handleInfoChange }) {
  const { values, handleBlur, handleChange, errors, touched } = props;

  useEffect(() => {
    const stepValues = {
      firstname: values.firstname,
      lastname: values.lastname,
      email: values.email,
      cpf: values.cpf,
      nasc: values.nasc,
      phone: values.phone
    };

    handleInfoChange(stepValues);
  }, [values]);

  const lastNameRef = useRef(null);
  const emailRef = useRef(null);
  const cpfRef = useRef(null);

  return (
    <>
      <Row>
        <Input
          flex={1}
          value={values.firstname}
          label="Nome"
          autoCapitalize="words"
          autocompleteType="name"
          onBlur={handleBlur("firstname")}
          placeholder="Ex: João"
          onChangeText={handleChange("firstname")}
          onSubmitEditing={() => lastNameRef.current.focus()}
        />
        {errors.name && touched.name && (
          <ErrorMessage>{errors.name}</ErrorMessage>
        )}
      </Row>

      <Row>
        <Input
          flex={1}
          label="Sobrenome"
          autoCapitalize="words"
          autocompleteType="name"
          placeholder="Ex: Silva"
          inputRef={lastNameRef}
          value={values.lastname}
          onBlur={handleBlur("lastname")}
          onChangeText={handleChange("lastname")}
          onSubmitEditing={() => emailRef.current.focus()}
        />
        {errors.name && touched.name && (
          <ErrorMessage>{errors.name}</ErrorMessage>
        )}
      </Row>

      <Row>
        <Input
          flex={1}
          label="E-mail"
          value={values.email}
          autocompleteType="email"
          inputRef={emailRef}
          keyboardType="email-address"
          onBlur={handleBlur("email")}
          placeholder="Ex: joao@mail.com"
          onChangeText={handleChange("email")}
          onSubmitEditing={() => cpfRef.current.focus()}
        />
        {errors.email && touched.email && (
          <ErrorMessage>{errors.email}</ErrorMessage>
        )}
      </Row>

      <Row>
        <View style={{ flex: 1 }}>
          <Input
            masked
            flex={1}
            type="cpf"
            label="CPF"
            inputRef={cpfRef}
            value={values.cpf}
            keyboardType="numeric"
            onBlur={handleBlur("cpf")}
            placeholder="Ex: 000.000.000-00"
            onChangeText={handleChange("cpf")}
            style={{ marginRight: metrics.baseMargin_1x }}
          />

          {errors.cpf && touched.cpf && (
            <ErrorMessage>{errors.cpf}</ErrorMessage>
          )}
        </View>

        <Input
          masked
          flex={0.65}
          type="custom"
          label="Data Nasc."
          value={values.nasc}
          placeholder="00/00/0000"
          onBlur={handleBlur("nasc")}
          options={{ mask: "99-99-9999" }}
          onChangeText={handleChange("nasc")}
          keyboardType={Platform.OS === "android" ? "number-pad" : "default"}
        />

        {errors.nasc && touched.nasc && (
          <ErrorMessage>{errors.nasc}</ErrorMessage>
        )}
      </Row>

      <Row>
        <View style={{ flex: 0.5 }}>
          <Input
            masked
            flex={1}
            type="cel-phone"
            value={values.phone}
            autocompleteType="tel"
            label="Telefone/Celular"
            keyboardType={Platform.OS === "android" ? "number-pad" : "default"}
            onBlur={handleBlur("phone")}
            placeholder="Ex: 15 3537 9110"
            onChangeText={handleChange("phone")}
            onSubmitEditing={() => Keyboard.dismiss()}
          />

          {errors.phone && touched.phone && (
            <ErrorMessage>{errors.phone}</ErrorMessage>
          )}
        </View>
      </Row>
    </>
  );
}
