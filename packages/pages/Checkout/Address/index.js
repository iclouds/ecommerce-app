import React, { useEffect, useRef, useState } from 'react'
import { View, ActivityIndicator } from 'react-native'

import { api } from 'services'
import { metrics } from 'theme'
import { Input } from 'components'
import { ConvertFloatToMoney } from 'functions'

import {
  Row,
  ErrorMessage,
  Field,
  RequestStatus,
  RequestError,
} from '../styles'
import {
  FreteContainer,
  Title,
  Option,
  Value,
  Description,
  Options,
} from './styles'

function Address({
  props,
  colors,
  loading,
  freteType,
  setFreteType,
  handleCepSearch,
  handleAddressChange,
  addressInformation,
  setAddressInformation,

  freteItems,
  fetchFretes,
}) {
  const { values, handleBlur, handleChange, errors, touched } = props

  const numberRef = useRef(null)
  const complementRef = useRef(null)

  useEffect(() => {
    fetchFretes(props.values)
  }, [])

  useEffect(() => {
    const stepValues = values.address

    handleAddressChange(stepValues)
  }, [values])

  function handleFretePrice(frete) {
    if (frete === 0) return 'Grátis'

    return ConvertFloatToMoney(frete.toString())
  }

  return (
    <View>
      <Row>
        <Field flex={0.65}>
          <Input
            masked
            flex={1}
            label="CEP"
            type="custom"
            keyboardType="numeric"
            value={values.address.cep}
            placeholder="Ex: 00000-000"
            options={{ mask: '99999-999' }}
            onChangeText={handleChange('address.cep')}
            onSubmitEditing={() => numberRef.current.focus()}
            onBlur={() => (
              handleBlur('address.cep'),
              handleCepSearch(values.address.cep, values)
            )}
          />

          {errors.address && touched.address && touched.address.cep && (
            <ErrorMessage>{errors.address.cep}</ErrorMessage>
          )}
        </Field>

        <RequestStatus>
          {addressInformation.loading && (
            <ActivityIndicator size="small" color={colors.secondaryVariant} />
          )}
          {addressInformation.error && (
            <RequestError>CEP inválido!</RequestError>
          )}
        </RequestStatus>
      </Row>

      <Row>
        <Input
          flex={1}
          label="Endereço"
          value={values.address.street}
          placeholder="Ex: R. Saldanha Marinho"
          onBlur={handleBlur('address.street')}
          onChangeText={handleChange('address.street')}
        />

        {errors.address && touched.address && touched.address.street && (
          <ErrorMessage>{errors.address.street}</ErrorMessage>
        )}
      </Row>

      <Row>
        <Field flex={1}>
          <Input
            flex={1}
            label="Cidade"
            placeholder="Ex: Itapetininga"
            value={values.address.city}
            onBlur={handleBlur('address.city')}
            onChangeText={handleChange('address.city')}
            style={{ marginRight: metrics.baseMargin_1x }}
          />

          {errors.address && touched.address && touched.address.city && (
            <ErrorMessage>{errors.address.city}</ErrorMessage>
          )}
        </Field>

        <Field flex={0.65}>
          <Input
            flex={1}
            label="Estado"
            placeholder="Ex: SP"
            value={values.address.state}
            onBlur={handleBlur('address.state')}
            onChangeText={handleChange('address.state')}
          />

          {errors.address && touched.address && touched.address.state && (
            <ErrorMessage>{errors.address.state}</ErrorMessage>
          )}
        </Field>
      </Row>

      <Row>
        <Field flex={0.65}>
          <Input
            flex={1}
            label="Número"
            inputRef={numberRef}
            placeholder="Ex: 999"
            value={values.address.number}
            onBlur={handleBlur('address.number')}
            onSubmitEditing={() => complementRef.current.focus()}
            onChangeText={text => (
              handleChange('address.number'),
              setAddressInformation({
                ...addressInformation,
                data: { ...addressInformation.data, number: text },
              })
            )}
            style={{ marginRight: metrics.baseMargin_1x }}
          />

          {errors.address && touched.address && touched.address.number && (
            <ErrorMessage>{errors.address.number}</ErrorMessage>
          )}
        </Field>

        <Field flex={1}>
          <Input
            flex={1}
            label="Bairro"
            placeholder="Ex: Centro"
            value={values.address.neighborhood}
            onBlur={handleBlur('address.neighborhood')}
            onChangeText={handleChange('address.neighborhood')}
          />

          {errors.address &&
            touched.address &&
            touched.address.neighborhood && (
              <ErrorMessage>{errors.address.neighborhood}</ErrorMessage>
            )}
        </Field>
      </Row>

      <Row>
        <Field flex={1}>
          <Input
            flex={1}
            label="Complemento"
            placeholder="Ex: Casa"
            inputRef={complementRef}
            value={values.address.complement}
            onBlur={handleBlur('address.complement')}
            onChangeText={text => (
              handleChange('address.complement'),
              setAddressInformation({
                ...addressInformation,
                data: { ...addressInformation.data, complement: text },
              })
            )}
          />

          {errors.address && touched.address && touched.address.complement && (
            <ErrorMessage>{errors.address.complement}</ErrorMessage>
          )}
        </Field>
      </Row>

      <Row style={{ marginTop: 20, height: 90 }}>
        <FreteContainer>
          <Title>Frete e entrega</Title>

          {loading ? (
            <ActivityIndicator color={colors.secondary} />
          ) : (
            <Options>
              {freteItems &&
                freteItems.map((item, index) => (
                  <Option
                    key={item.id}
                    first={index === 0}
                    selected={freteType.price === item.price}
                    onPress={() => setFreteType(item)}
                  >
                    <Value>{handleFretePrice(item.price)}</Value>
                    <Description>Até {item.term} dias úteis</Description>
                  </Option>
                ))}
            </Options>
          )}
        </FreteContainer>
      </Row>
    </View>
  )
}

export default Address
