import styled from 'styled-components/native'

import { fonts, colors, metrics } from 'theme'

export const FreteContainer = styled.View`
  width: 100%;
`

export const Title = styled(fonts.size_3x)`
  margin-bottom: ${metrics.baseMargin_1x}px;

  font-weight: 500;
  color: ${colors.grey};
`

export const Options = styled.ScrollView.attrs({
  horizontal: true,
  showsHorizontalScrollIndicator: false,
})`
  width: ${metrics.screenWidth};
  margin-left: -${metrics.baseContainer};

  flex-direction: row;
`

export const Option = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})`
  padding: 12px;
  margin-left: ${props =>
    props.first ? metrics.baseContainer : metrics.baseMargin_1x}px;
  border-radius: ${metrics.baseRadius}px;

  flex-direction: column;
  align-items: center;
  justify-content: center;

  background-color: ${props =>
    props.selected ? props.theme.secondary : props.theme.secondaryVariant};
`

export const Value = styled(fonts.size_1x)`
  font-weight: 500;
  color: ${colors.white};
  letter-spacing: -0.03px;
`

export const Description = styled(fonts.size_0x)`
  color: ${colors.white};
`
