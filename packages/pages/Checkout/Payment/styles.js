import styled from 'styled-components/native';

import { fonts, colors } from 'theme';

export const Container = styled.View``;

export const Divider = styled.View`
  height: 1px;
  width: 100%;

  align-items: center;

  background: ${colors.greyest};
`;

export const Description = styled(fonts.size_0x)`
  padding: 6px;
  margin-top: -4px;

  background: ${colors.white};
  line-height: 8px;
  color: ${props => props.theme.secondaryVariant};
`;
