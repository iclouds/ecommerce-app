import React, { useRef } from "react";
import PropTypes from "prop-types";
import { View, Platform } from "react-native";

import { metrics } from "theme";
import { ConvertFloatToMoney } from "functions";
import { Input, Button, Select } from "components";

import { Divider, Description } from "./styles";
import { Row, ErrorMessage, Field } from "../styles";

export default function Payment({
  props,
  colors,
  boleto,
  setBoleto,
  handleSteps,
  installments,
  selectedInstallment,
  setSelectedInstallment
}) {
  const { values, handleBlur, handleChange, errors, touched } = props;

  const cvvRef = useRef(null);
  const nameRef = useRef(null);
  const yearRef = useRef(null);
  const monthRef = useRef(null);

  function formatedInstallments() {
    return installments.map(
      option =>
        `${option.installment_number}x de ${ConvertFloatToMoney(
          Number(option.installment_value).toFixed(2)
        )}`
    );
  }

  return (
    <View style={{ paddingBottom: Platform.OS === "android" ? 60 : 0 }}>
      <Row style={{ justifyContent: "center" }}>
        <Button
          hasIcon
          text="Boleto"
          isChecked={boleto}
          onPress={() => setBoleto(!boleto)}
          color={boleto && colors.secondaryVariant}
        />
      </Row>

      <Row>
        <Divider>
          <Description>OU</Description>
        </Divider>
      </Row>

      <Row>
        <Input
          masked
          flex={1}
          type="custom"
          keyboardType="numeric"
          label="Número do cartão"
          editable={boleto ? false : true}
          placeholder="Ex: 0000 0000 0000 0000"
          value={values.payment.credit_card.number}
          options={{ mask: "9999 9999 9999 9999" }}
          onBlur={handleBlur("payment.credit_card.number")}
          onChangeText={handleChange("payment.credit_card.number")}
          onSubmitEditing={() => nameRef.current.focus()}
        />
        {errors.payment &&
          touched.payment &&
          touched.payment.credit_card.number && (
            <ErrorMessage>{errors.payment.credit_card.number}</ErrorMessage>
          )}
      </Row>

      <Row>
        <Input
          flex={1}
          inputRef={nameRef}
          label="Nome impresso no cartão"
          placeholder="Ex: Joao da Silva"
          editable={boleto ? false : true}
          value={values.payment.credit_card.name}
          onBlur={handleBlur("payment.credit_card.name")}
          onChangeText={handleChange("payment.credit_card.name")}
          onSubmitEditing={() => monthRef.current.focus()}
        />
        {errors.payment &&
          touched.payment &&
          touched.payment.credit_card.name && (
            <ErrorMessage>{errors.payment.credit_card.name}</ErrorMessage>
          )}
      </Row>

      <Row>
        <Field flex={1}>
          <Input
            masked
            flex={1}
            inputRef={monthRef}
            label="Mês"
            type="custom"
            placeholder="Ex: 05"
            keyboardType="numeric"
            options={{ mask: "99" }}
            editable={boleto ? false : true}
            value={values.payment.credit_card.month}
            style={{ marginRight: metrics.baseMargin_1x }}
            onBlur={handleBlur("payment.credit_card.month")}
            onChangeText={handleChange("payment.credit_card.month")}
            onSubmitEditing={() => yearRef.current.focus()}
          />
          {errors.payment &&
            touched.payment &&
            touched.payment.credit_card.month && (
              <ErrorMessage>{errors.payment.credit_card.month}</ErrorMessage>
            )}
        </Field>

        <Field flex={1}>
          <Input
            masked
            flex={1}
            label="Ano"
            type="custom"
            inputRef={yearRef}
            placeholder="Ex: 21"
            keyboardType="numeric"
            options={{ mask: "99" }}
            editable={boleto ? false : true}
            value={values.payment.credit_card.year}
            style={{ marginRight: metrics.baseMargin_1x }}
            onSubmitEditing={() => cvvRef.current.focus()}
            onBlur={handleBlur("payment.credit_card.year")}
            onChangeText={handleChange("payment.credit_card.year")}
          />
          {errors.payment &&
            touched.payment &&
            touched.payment.credit_card.year && (
              <ErrorMessage>{errors.payment.credit_card.year}</ErrorMessage>
            )}
        </Field>

        <Field flex={1}>
          <Input
            masked
            flex={1}
            label="CVV"
            type="custom"
            inputRef={cvvRef}
            keyboardType="numeric"
            placeholder="Ex: 000"
            options={{ mask: "9999" }}
            editable={boleto ? false : true}
            value={values.payment.credit_card.cvv}
            onBlur={handleBlur("payment.credit_card.cvv")}
            onChangeText={handleChange("payment.credit_card.cvv")}
          />
          {errors.payment &&
            touched.payment &&
            touched.payment.credit_card.cvv && (
              <ErrorMessage>{errors.payment.credit_card.cvv}</ErrorMessage>
            )}
        </Field>
      </Row>

      {!boleto && (
        <>
          <Row>
            <Divider>
              <Description>Como deseja pagar ?</Description>
            </Divider>
          </Row>

          <Row>
            <Select
              name="Parcelas"
              colors={colors}
              items={formatedInstallments()}
              setSelectItem={handleChange("payment.credit_card.installments")}
            />
          </Row>
        </>
      )}
    </View>
  );
}

Payment.defaultProps = {
  boleto: false
};

Payment.propTypes = {
  boleto: PropTypes.bool
};
