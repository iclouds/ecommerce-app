/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";
import { ScrollView, Keyboard, Platform } from "react-native";
import * as yup from "yup";
import { Formik } from "formik";
import { isIphoneX } from "react-native-iphone-x-helper";

import { cepApi, api } from "services";
import { ValidateCpf } from "functions";
import { Stepper, ActionFooter } from "components";
import { Profile, Address, Payment, Resume } from "pages";
import axios from "axios";

import { Container, StepperContainer } from "./styles";

function Checkout({
  cart,
  colors,
  baseurl,
  apiKeys,
  isLoading,
  handleBuySubmit,
  handleInfoChange,
  handleAddressChange,
  initialReduxUserInfo,
}) {
  const [currentStep, setCurrentStep] = useState(0);
  const [nextStepName, setNextStepName] = useState("");
  const [actionType, setActionType] = useState(null);
  const [isNextStepAvailable, setIsNextStepAvailable] = useState(false);

  const [userCpf, setUserCpf] = useState("");
  const [boleto, setBoleto] = useState(false);
  const [datepicker, setDatepicker] = useState("");
  const [freteItems, setFreteItems] = useState([]);
  const [selectedInstallment, setSelectedInstallment] = useState("1");
  const [installments, setInstallments] = useState([]);
  const [freteLoading, setFreteLoading] = useState(false);
  const [freteType, setFreteType] = useState({ price: 0 });

  const [isKeyboardShow, setIsKeyboardShow] = useState(false);

  const [persistedInformation, setPersistedInformation] = useState({
    ...initialReduxUserInfo,
    payment: {
      credit_card: {
        name: "",
        number: "",
        month: "",
        year: "",
        cvv: "",
      },
      frete: freteType,
    },
  });
  const [addressInformation, setAddressInformation] = useState({
    loading: false,
    error: false,
    data: initialReduxUserInfo.address,
  });

  const [allPurchaseData, setAllPurchaseData] = useState(null);

  useEffect(() => {
    function handleStepName() {
      if (currentStep === 0) return setNextStepName("ENDEREÇO DE ENTREGA");
      if (currentStep === 1) return setNextStepName("DADOS DE PAGAMENTO");
      if (currentStep === 2) return setNextStepName("CONFIRMAÇÃO");
      if (currentStep === 3) return setNextStepName("FINALIZAR COMPRA");

      return setNextStepName("HOME");
    }

    handleStepName();
  }, [currentStep]);

  useState(() => {
    Keyboard.addListener("keyboardWillShow", () => setIsKeyboardShow(true));
    Keyboard.addListener("keyboardWillHide", () => setIsKeyboardShow(false));
  }, []);

  async function fetchCep(cep, persistedValues) {
    setAddressInformation({
      ...addressInformation,
      loading: true,
      error: false,
    });

    try {
      const response = await cepApi.get(`${cep}/json`);

      if (response.data.erro) throw new Error("failed");

      const address = {
        cep: response.data.cep,
        street: response.data.logradouro,
        neighborhood: response.data.bairro,
        city: response.data.localidade,
        state: response.data.uf,
      };

      setPersistedInformation({ ...persistedValues });

      setAddressInformation({
        ...addressInformation,
        data: { ...address },
        loading: false,
        error: false,
      });
    } catch (error) {
      setAddressInformation({
        ...addressInformation,
        error: true,
        loading: false,
      });
    }
  }

  async function handleCepSearch(cep, persistedValues) {
    if (cep.length < 9)
      return setAddressInformation({ ...addressInformation, error: true });

    await fetchCep(cep, persistedValues);

    return true;
  }

  async function fetchFretes(persistedValues) {
    setFreteLoading(true);

    try {
      const freteParams = cart.map((item) => {
        return {
          product_id: item.id,
          quantity: item.amount,
          shipping_class_id: item.shipping_class_id,
          dimensions: item.dimensions,
          price: Number(item.price),
        };
      });

      const {
        data: { shipping, installments },
      } = await axios.post(
        `${baseurl}checkout-options`,
        { products: freteParams },
        {
          auth: {
            username: apiKeys.username,
            password: apiKeys.password,
          },
        }
      );

      setPersistedInformation({ ...persistedValues });
      setFreteType(shipping[0]);
      setFreteItems(shipping);
      setInstallments(installments);
    } catch (err) {
      alert("(Checkout) Algo deu errado!");
    } finally {
      setFreteLoading(false);
    }
  }

  function isLastStep(maxStep) {
    return currentStep === maxStep;
  }

  function isFirstStep() {
    return currentStep === 0;
  }

  function handleSteps(type) {
    if (type === "next") {
      if (!isLastStep(3)) {
        setCurrentStep(currentStep + 1);
        return setActionType({ type: "next" });
      }

      handleBuySubmit(allPurchaseData);

      return null;
    }

    if (!isFirstStep()) {
      setCurrentStep(currentStep - 1);
      return setActionType({ type: "back" });
    }

    return null;
  }

  function validationSchema() {
    return yup.object().shape({
      firstname: yup.string().required("campo requerido."),
      lastname: yup.string().required("campo requerido."),
      phone: yup
        .string()
        .min(14, "número incorreto")
        .required("campo requerido."),
      email: yup
        .string()
        .email("insira um e-mail válido.")
        .required("campo requerido."),
      cpf: yup
        .string()
        .test("cpf-validate", "cpf inválido", () => ValidateCpf(userCpf))
        .required("campo requerido."),
      nasc: yup.string().required("campo requerido"),
      payment: yup.object({
        credit_card: yup.object({
          name: yup.string().required("campo requerido."),
          number: yup
            .string()
            .required("campo requerido.")
            .min(19, "número incorreto"),
          month: yup.string().required("campo requerido."),
          year: yup.string().required("campo requerido."),
          cvv: yup.string().required("campo requerido."),
        }),
      }),
      address: yup.object({
        cep: yup.string().required("campo requerido."),
        street: yup.string().required("campo requerido."),
        city: yup.string().required("campo requerido."),
        state: yup.string().required("campo requerido."),
        number: yup.string().required("campo requerido."),
        neighborhood: yup.string().required("campo requerido."),
        complement: yup.string(),
      }),
    });
  }

  function renderFirstStep(props) {
    const { values, errors } = props;

    setUserCpf(values.cpf);
    setDatepicker(values.nasc);

    function isAbleToNextStep() {
      return (
        !errors.name &&
          !errors.email &&
          !errors.cpf &&
          !errors.nasc &&
          values.name !== "",
        values.email !== "",
        values.cpf !== "",
        values.nasc !== "",
        values.phone !== "" && !errors.phone
      );
    }

    if (isAbleToNextStep()) {
      setIsNextStepAvailable(true);
    } else {
      setIsNextStepAvailable(false);
    }

    return (
      <Profile
        props={props}
        datepicker={datepicker}
        handleInfoChange={handleInfoChange}
      />
    );
  }

  function renderSecondStep(props) {
    const { errors, values } = props;

    if (!errors.address && values.address.number) {
      setIsNextStepAvailable(true);
    } else {
      setIsNextStepAvailable(false);
    }

    return (
      <Address
        cart={cart}
        colors={colors}
        props={props}
        colors={colors}
        freteType={freteType}
        loading={freteLoading}
        freteItems={freteItems}
        fetchFretes={fetchFretes}
        setFreteType={setFreteType}
        handleCepSearch={handleCepSearch}
        handleAddressChange={handleAddressChange}
        addressInformation={addressInformation}
        setAddressInformation={setAddressInformation}
      />
    );
  }

  function renderThirdStep(props) {
    const { values, errors } = props;

    function isAbleToNextStep() {
      return (
        (!errors.payment &&
          values.payment.credit_card.name !== "" &&
          values.payment.credit_card.number !== "" &&
          values.payment.credit_card.month !== "" &&
          values.payment.credit_card.year !== "" &&
          values.payment.credit_card.cvv !== "") ||
        values.payment.boleto === true
      );
    }

    if (isAbleToNextStep()) {
      setIsNextStepAvailable(true);
    } else {
      setIsNextStepAvailable(false);
    }

    return (
      <Payment
        props={props}
        colors={colors}
        boleto={boleto}
        setBoleto={setBoleto}
        handleSteps={handleSteps}
        installments={installments}
        selectedInstallment={selectedInstallment}
        setSelectedInstallment={setSelectedInstallment}
      />
    );
  }

  function renderFourthStep(props) {
    setAllPurchaseData(props.values);

    return <Resume props={props} />;
  }

  function renderStepContent(props) {
    if (currentStep === 0) return renderFirstStep(props);
    if (currentStep === 1) return renderSecondStep(props);
    if (currentStep === 2) return renderThirdStep(props);
    if (currentStep === 3) return renderFourthStep(props);

    return renderFirstStep(props);
  }

  const formInitialValues = {
    ...persistedInformation,
    address: {
      ...addressInformation.data,
    },
    payment: {
      credit_card: {
        name: "",
        number: "",
        month: "",
        year: "",
        cvv: "",
        installments: "1",
      },
      boleto,
      frete: freteType,
    },
  };

  return (
    <>
      <Container>
        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
            marginTop:
              isKeyboardShow && Platform.OS === "ios"
                ? isIphoneX()
                  ? 180
                  : 120
                : 0,
          }}
        >
          <StepperContainer>
            <Stepper action={actionType} />
          </StepperContainer>

          <Formik
            enableReinitialize
            validateOnChange={false}
            initialValues={formInitialValues}
            validationSchema={validationSchema()}
          >
            {(props) => renderStepContent(props)}
          </Formik>
        </ScrollView>
      </Container>

      <ActionFooter
        monoIcon
        primaryText={(isLoading && "FINALIZANDO PEDIDO...") || nextStepName}
        disabled={!isNextStepAvailable || isLoading}
        primaryActionOnPress={() =>
          (isLoading && "Realizando pedido...") || handleSteps("next")
        }
        secondaryActionOnPress={() =>
          (isLoading && "Realizando pedido...") || handleSteps("back")
        }
      />
    </>
  );
}

export default Checkout;
