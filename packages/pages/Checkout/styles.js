import styled from 'styled-components/native'
import { Platform } from 'react-native'

import { metrics, fonts, colors } from 'theme'

export const Container = styled.KeyboardAvoidingView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'position',
})`
  flex: 1;
  padding: ${metrics.baseContainer}px;
  height: ${metrics.screenHeight}px;
`

export const StepperContainer = styled.View`
  margin-bottom: ${metrics.baseMargin_3x}px;
`

export const Row = styled.View`
  height: 60px;
  margin-bottom: ${metrics.baseMargin_1x}px;

  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`

export const Field = styled.View`
  flex: ${props => props.flex};
`

export const ErrorMessage = styled(fonts.size_0x)`
  position: absolute;
  right: 8px;
  bottom: -12px;

  color: ${colors.danger};
`

export const RequestError = styled(fonts.size_0x)`
  color: ${colors.danger};
`

export const RequestStatus = styled.View`
  height: 60px;
  padding-bottom: 12px;

  flex: 1;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-start;
`
