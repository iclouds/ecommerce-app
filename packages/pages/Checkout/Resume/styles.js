import styled from 'styled-components/native';
import { isIphoneX } from 'react-native-iphone-x-helper';

import { metrics } from 'theme';

export const Container = styled.ScrollView`
  width: ${metrics.screenWidth};
  margin: -${metrics.baseContainer}px 0 ${!isIphoneX() ? 40 : 70}px -${metrics.baseContainer}px ;
`;
