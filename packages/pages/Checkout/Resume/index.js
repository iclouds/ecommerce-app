/* eslint-disable react/no-array-index-key */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { Card } from "components";
import { ConvertFloatToMoney } from "functions";

import { Container } from "./styles";

function Resume({ cart, props }) {
  const { values } = props;
  const price = values.payment.frete.price;

  function freteInformation() {
    const term = values.payment.frete.term;

    if (price !== 0)
      return [
        {
          title: "Frete e entrega",
          description: `${ConvertFloatToMoney(
            price.toString()
          )} - Até ${term} dias úteis`
        }
      ];

    return [
      {
        title: "Frete e entrega",
        description: `Gratís - Até ${term} dias úteis`
      }
    ];
  }

  function addressInformation() {
    const { address } = values;
    const information = [];

    function itemTranslate(item) {
      switch (item) {
        case "cep":
          return "Cep";
        case "street":
          return "Rua";
        case "neighborhood":
          return "Bairro";
        case "city":
          return "Cidade";
        case "state":
          return "Estado";
        case "number":
          return "Número";
        case "complement":
          return "Complemento";
        default:
          return "";
      }
    }

    Object.keys(address).map(item => {
      const title = itemTranslate(item);

      information.push({ title, description: address[item] });
    });

    return information;
  }

  function priceInformation() {
    const productsValues = cart.map(item => item.amount * item.price);
    const totalProductsValues = productsValues.reduce(
      (item, accumulator) => item + accumulator,
      0
    );

    const frete = price;

    const totalWithFrete = (
      Number(totalProductsValues) + Number(frete)
    ).toFixed(2);

    return [
      {
        title: "Total de produtos",
        description: `${ConvertFloatToMoney(
          totalProductsValues.toString()
        )} em ${values.payment.credit_card.installments}x`
      },
      { title: "Frete", description: ConvertFloatToMoney(frete.toString()) },
      {
        title: "Total da compra",
        description: ConvertFloatToMoney(totalWithFrete.toString())
      }
    ];
  }

  return (
    <Container>
      {cart &&
        cart.map(item => <Card key={item.cartId} productProps={{ ...item }} />)}

      <Card listProps={freteInformation()} />

      <Card listProps={addressInformation()} />

      <Card isTotal listProps={priceInformation()} />
    </Container>
  );
}

Resume.propTypes = {
  cart: PropTypes.arrayOf(PropTypes.shape()).isRequired
};

const mapStateToProps = state => ({
  cart: state.cart.data
});

export default connect(mapStateToProps)(Resume);
