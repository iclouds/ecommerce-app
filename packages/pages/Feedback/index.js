import React from 'react';

import { ActionFooter } from 'components';

import {
  Container, CustomIcon, Title, Description, Strong,
} from './styles';

function Feedback({ message, isSuccess, purchaseId, handleFinishCashout }) {
  function renderSuccess(purchaseId) {
    return (
      <Container>
        <CustomIcon type="success" />

        <Title>Obrigado</Title>

        <Description>
          Seu pedido <Strong>#{purchaseId}</Strong> foi recebido, confira em seu email os dados de confirmação.
        </Description>
      </Container>
    );
  }

  function renderFailure(message) {
    return (
      <Container>
        <CustomIcon type="failure" />

        <Title>Desculpe</Title>

        <Description>
          { message || "Seu pedido não foi recebido, tente novamente." }
        </Description>
      </Container>
    );
  }

  return (
    <>
      {isSuccess && renderSuccess(purchaseId)}
      {!isSuccess && renderFailure(message)}

      <ActionFooter
        monoAction
        primaryText="PÁGINA INICIAL"
        primaryActionOnPress={() => handleFinishCashout()}
      />
    </>
  );
}

export default Feedback;
