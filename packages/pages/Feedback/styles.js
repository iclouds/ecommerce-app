import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/FontAwesome';

import { fonts, metrics, colors } from 'theme';

export const Container = styled.View`
  flex: 1;
  align-content: center;
  justify-content: center;
  padding: ${metrics.baseContainer * 2}px;
`;

export const CustomIcon = styled(Icon).attrs(props => ({
  size: 120,
  name: props.type === 'success' ? 'check-circle' : 'times-circle',
  color: props.type === 'success' ? colors.success : colors.danger,
}))`
  margin: 0 auto ${metrics.baseMargin_2x}px;
`;

export const Title = styled(fonts.size_4x)`
  margin-bottom: ${metrics.baseMargin_1x}px;

  line-height: 21px;
  font-weight: bold;
  text-align: center;
`;

export const Description = styled(fonts.size_1x)`
  text-align: center;
  color: ${colors.grey};
`;

export const Strong = styled(Description)`
  font-weight: bold;
`;
