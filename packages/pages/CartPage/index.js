import React, { useState, useEffect } from 'react'

import { useSelector } from 'react-redux';

import PropTypes from 'prop-types'
import { FlatList } from 'react-native'
import { withNavigation } from 'react-navigation'

import { ProductCart, ActionFooter } from 'components'
import { colors } from 'theme'

import { Container, ErrorMessage } from './styles'

import "react-native-easy-toast";
import ToastNotification from 'react-native-toast-notification'

function CartPage({
  products,
  navigation,
  removeFromCart,
  editProduct,
  isLoggedIn,
  stockProducts,
  isLoading,
  handleAvailableProducts
}) {
  const [page, setPage] = useState(1)
  const [perPage, setPerPage] = useState(100)
  const [isAbleToProceed, setIsAbleToProceed] = useState(false)
  const [hasUnavailable, setHasUnavailable] = useState(false)
  const [cartProducts, setCartProducts] = useState(products)
  const [userProceed, setUserProceed] = useState(false)

  useEffect(() => {
    handleAbleToProceed();
    handleStock()
  }, [stockProducts])

  function handleAbleToProceed(hasUnavailable = hasUnavailable){
    if (products.length > 0 && !hasUnavailable) {
      setIsAbleToProceed(true)
    } else {
      setIsAbleToProceed(false)
    }
  }

  async function handleStock(){
    if(!products.length > 0 || !stockProducts.length > 0){
      return;
    }

    const verifiedProducts = await products.map((cartProduct, index) => {
      const product = stockProducts.find(product => {
        return product.id === cartProduct.id
      })

      const amountInStock = Number(product.stock_quantity);
      const amountInCart = Number(cartProduct.amount);

      const isUnavailable = [false, 0, null].includes(amountInStock) || amountInCart > amountInStock;

      cartProduct.available = !isUnavailable;

      return cartProduct;
    })

    const hasUnavailableItems = verifiedProducts.filter(product => product.available === false).length > 0;

    setHasUnavailable(hasUnavailableItems);
    setCartProducts(verifiedProducts)
    handleAbleToProceed(hasUnavailableItems)

    if(userProceed && !hasUnavailableItems) {
      handleUserAuth()
    }
  }

  function handleUserAuth() {
    return isLoggedIn
      ? navigation.navigate('Checkout')
      : navigation.navigate('Sign')
  }

  function renderToast(){
    return <ToastNotification
      textStyle={{ color: '#fff' }}
      isTop={true}
      duration={10000}
      style={{ backgroundColor: colors.danger, padding: 35 }}
      text="Oops! alguns produtos estão indisponíveis, remova do carrinho para continuar."
      onHide={this._handleOnHideToastNotification}
    />
  }

  function renderProducts() {
    return (
      <FlatList
        data={cartProducts}
        showsVerticalScrollIndicator={false}
        keyExtractor={item => String(item.cartId)}
        renderItem={product => (
          <ProductCart
            product={product}
            editProduct={() =>
              editProduct({
                id: product.item.id,
                amount: product.item.amount,
                type: 'sum',
              })
            }
            removeItem={() => removeFromCart(product.item.cartId)}
          />
        )}
      />
    )
  }

  return (
    <>
      <Container>
        {products.length > 0 ? (
          renderProducts()
        ) : (
          <ErrorMessage>Sem produtos adicionados</ErrorMessage>
        )}

        { hasUnavailable && renderToast() }
      </Container>

      <ActionFooter
        monoIcon
        monoAction
        disabled={!isAbleToProceed}
        isLoading={isLoading}
        primaryText="PROSSEGUIR COM A COMPRA"
        primaryActionOnPress={() => {
          handleAvailableProducts(handleUserAuth)
          setUserProceed(true)
        }}
      />
    </>
  )
}

CartPage.propTypes = {
  products: PropTypes.array.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
  removeFromCart: PropTypes.func.isRequired,
}

export default withNavigation(CartPage)
