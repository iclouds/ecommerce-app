import styled from 'styled-components/native'

import { metrics, fonts } from 'theme'

export const Container = styled.View`
  padding: ${metrics.baseContainer}px;
  padding-bottom: 0;
`

export const ErrorMessage = styled(fonts.size_2x)`
  text-align: center;
`
