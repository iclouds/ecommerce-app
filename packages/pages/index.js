import Checkout from './Checkout'
import CartPage from './CartPage'
import Feedback from './Feedback'
import Resume from './Checkout/Resume'
import Address from './Checkout/Address'
import Payment from './Checkout/Payment'
import Profile from './Checkout/Profile'
import ProductDetail from './ProductDetail'
import ListingProducts from './ListingProducts'

export {
  Resume,
  Address,
  Payment,
  Profile,
  CartPage,
  Checkout,
  Feedback,
  ProductDetail,
  ListingProducts,
}
