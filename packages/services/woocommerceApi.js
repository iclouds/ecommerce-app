import WooCommerceAPI from "react-native-woocommerce-api";

function connectApi(consumerKey, consumerSecret, baseUrl) {
  const wooCommerce = new WooCommerceAPI({
    url: baseUrl,
    ssl: true,
    consumerKey,
    consumerSecret,
    wpAPI: true,
    version: "wc/v3",
    queryStringAuth: true
  });

  return wooCommerce;
}

function getAllProducts(consumerKey, consumerSecret, baseUrl, params) {
  const products = connectApi(consumerKey, consumerSecret, baseUrl).get(
    "products",
    params
  );

  return products;
}

function getAllProductVariations(consumerKey, consumerSecret, baseUrl, productId) {
  const products = connectApi(consumerKey, consumerSecret, baseUrl).get(
    `products/${productId}/variations`
  );

  return products;
}

function getProduct(consumerKey, consumerSecret, baseUrl, productId) {
  const products = connectApi(consumerKey, consumerSecret, baseUrl).get(
    `products/${productId}`
  );

  return products;
}

function getVariation(consumerKey, consumerSecret, baseUrl, productId, variationId) {
  const products = connectApi(consumerKey, consumerSecret, baseUrl).get(
    `products/${productId}/variations/${variationId}`
  );

  return products;
}

function getAllCategories(consumerKey, consumerSecret, baseUrl) {
  const productsCategories = connectApi(
    consumerKey,
    consumerSecret,
    baseUrl
  ).get("products/categories");

  return productsCategories;
}

function getAllFeaturedCategories(consumerKey, consumerSecret, baseUrl) {
  const productsCategories = connectApi(
    consumerKey,
    consumerSecret,
    baseUrl
  ).get("featured-category", {
    location: 'cutter_featured_category'
  });

  return productsCategories;
}


function getAllFooterCategories(consumerKey, consumerSecret, baseUrl) {
  const productsCategories = connectApi(
    consumerKey,
    consumerSecret,
    baseUrl
  ).get("featured-category", {
    location: 'cutter_footer_category'
  });

  return productsCategories;
}

function signUpUser(consumerKey, consumerSecret, baseUrl, userData) {
  const newUser = connectApi(consumerKey, consumerSecret, baseUrl).post(
    "customers",
    userData
  );

  return newUser;
}

function updateUser(consumerKey, consumerSecret, baseUrl, userData) {
  const updatedUser = connectApi(consumerKey, consumerSecret, baseUrl).put(
    `customers/${userData.id}`,
    userData
  );

  return updatedUser;
}

export default {
  signUpUser,
  updateUser,
  getProduct,
  getVariation,
  getAllProducts,
  getAllCategories,
  getAllFooterCategories,
  getAllProductVariations,
  getAllFeaturedCategories
};
