import axios from 'axios'

const api = (baseURL, auth) => (
  axios.create({
    baseURL,
    auth
  })
)

const cepApi = axios.create({
  baseURL: 'https://viacep.com.br/ws/',
})

export { api, cepApi }
