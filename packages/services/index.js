import { api, cepApi } from './api'
import woocommerceApi from './woocommerceApi'

export { api, woocommerceApi, cepApi }
