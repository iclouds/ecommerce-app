//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Efraim Andrade on 27/11/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
