import { colors } from 'theme';

export default {
  primary: '#000',
  secondary: '#7A1347',
  secondaryVariant: '#94436b',
  secondaryLight: '#f3eaef',
  ...colors,
};
