import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { CartPage } from 'pages';
import { HeaderRight } from 'components';

import apiKeys from '../../config/ApiKeys';
import { woocommerceApi } from 'services';


function Cart() {
  const dispatch = useDispatch();

  const products = useSelector(state => state.cart.data);
  const isLoggedIn = useSelector(state => state.user.isLoggedIn);

  const [stockProducts, setStockProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(false)

  async function handleAvailableProducts() {
    setIsLoading(true)

    try {
      const apiCalls = products.map(product => {
        if(product.type === 'variable'){
          return woocommerceApi.getVariation(
            apiKeys.username,
            apiKeys.password,
            apiKeys.urlSdk,
            product.id,
            product.parent_id
          );
        }

        return woocommerceApi.getProduct(
          apiKeys.username,
          apiKeys.password,
          apiKeys.urlSdk,
          product.id
        )
      })

      const response = await Promise.all(apiCalls)

      setStockProducts(response)
    } catch (error) {
      alert('Deu ruim!')
    } finally {
      setIsLoading(false)
    }
  }


  useEffect(() => {
    handleAvailableProducts();
  }, [])

  const actions = {
    editProduct: data => dispatch({ type: 'GET_PRODUCTS_EDIT', data }),
    removeFromCart: data => dispatch({ type: 'REMOVE_FROM_CART', data }),
  };

  return (
    <CartPage
      {...actions}
      isLoggedIn={isLoggedIn}
      products={products}
      stockProducts={stockProducts}
      isLoading={isLoading}
      handleAvailableProducts={handleAvailableProducts}
    />
  );
}

export default {
  screen: Cart,
  navigationOptions: ({ navigation }) => ({
    title: 'Carrinho',
    headerRight: (
      <HeaderRight
        cartItems={1}
        iconNames={['home-outline']}
        onPressIcons={[() => navigation.navigate('Home')]}
      />
    ),
  }),
};
