import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { withNavigation } from 'react-navigation';

import { Feedback as FeedbackPage } from 'pages';

function Feedback({ navigation }) {
  const dispatch = useDispatch();

  function handleFinishCashout() {
    navigation.navigate('Home');

    dispatch({ type: 'CLEAN_CART' });
  }

  const isSuccess = navigation.getParam('status') === 'success';
  const purchaseId = navigation.getParam('purchaseId');
  const message = navigation.getParam('message');

  return (
    <FeedbackPage
      message={message}
      isSuccess={isSuccess}
      purchaseId={purchaseId}
      handleFinishCashout={handleFinishCashout}
    />
  );
}

Feedback.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

Feedback.navigationOptions = {
  header: null,
};

export default withNavigation(Feedback);
