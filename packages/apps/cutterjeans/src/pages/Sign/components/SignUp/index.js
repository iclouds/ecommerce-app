import React from 'react';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { Alert } from 'react-native';
import { withNavigation } from 'react-navigation';
import { useSelector, useDispatch } from 'react-redux';

import { api } from 'services';

import { Form } from 'components';
import { InputFactory } from 'functions';
import { woocommerceApi } from 'services';

import apiKeys from '../../../../config/ApiKeys';

const schema = Yup.object().shape({
  email: Yup.string()
    .email('Email inválido!')
    .required('Insira um Email!'),
  password: Yup.string().required('Insira uma senha!'),
  confirmPassword: Yup.string().when('password', (password, field) =>
    password
      ? field
          .required()
          .oneOf([Yup.ref('password')], 'As senhas estão diferentes!')
      : field
  ),
});

function SignIn({ navigation }) {
  const dispatch = useDispatch();
  const userInfo = useSelector(state => state.user.data);

  const inputs = [
    {
      labelText: 'Email',
      type: 'email',
      keyboardType: 'email-address',
      autoCompleteType: 'email',
      autoCorrect: true,
      value: userInfo.email,
    },
    { labelText: 'Senha', type: 'password', secureTextEntry: true },
    {
      labelText: 'Confirmar senha',
      type: 'confirmPassword',
      secureTextEntry: true,
    },
  ];

  async function handleSubmit(userData) {
    try {
      const apiHeader = {
        auth: {
          username: apiKeys.username,
          password: apiKeys.password,
        },
      };

      const signUpResponse = await woocommerceApi.signUpUser(
        apiKeys.username,
        apiKeys.password,
        apiKeys.urlSdk,
        userData
      );

      if (
        signUpResponse.code &&
        signUpResponse.code === 'registration-error-email-exists'
      ) {
        return Alert.alert('Falhou', signUpResponse.message);
      }

      const woocommerceFormatData = {
        user_login: userData.email,
        user_password: userData.password,
      };

      const { data } = await api(apiKeys.url).post(
        'user-login',
        woocommerceFormatData,
        apiHeader
      );

      dispatch({
        type: 'SIGN_IN_SUCCESS',
        payload: data,
      });

      Alert.alert('Sucesso!', 'Cadatro realizado.');

      return navigation.goBack();
    } catch (error) {
      return Alert.alert('Error', 'Algo deu errado tente novamente!');
    }
  }

  return (
    <Form
      type="signup"
      schema={schema}
      submit={handleSubmit}
      buttonText="CADASTRAR"
      inputs={InputFactory(inputs)}
    />
  );
}

SignIn.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func,
  }).isRequired,
};

export default withNavigation(SignIn);
