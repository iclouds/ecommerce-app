import React from 'react';
import * as Yup from 'yup';
import { Alert } from 'react-native';
import { withNavigation } from 'react-navigation';
import { useSelector, useDispatch } from 'react-redux';

import { api } from 'services';
import { Form } from 'components';
import { InputFactory } from 'functions';

import apiKeys from '../../../../config/ApiKeys';

const schema = Yup.object().shape({
  email: Yup.string().email('Email inválido!').required('Insira um Email!'),
  password: Yup.string().required('Insira uma senha!'),
});

function SignIn({ navigation }) {
  const dispatch = useDispatch();
  const userInfo = useSelector((store) => store.user.data);

  const inputs = [
    {
      labelText: 'Email',
      type: 'email',
      keyboardType: 'email-address',
      autoCompleteType: 'email',
      autoCorrect: true,
      returnKeyType: 'next',
      value: userInfo.email,
    },
    {
      labelText: 'Senha',
      type: 'password',
      secureTextEntry: true,
      returnKeyType: 'send',
    },
  ];

  async function handleSubmit({ email, password }) {
    try {
      const apiHeader = {
        auth: {
          username: apiKeys.username,
          password: apiKeys.password,
        },
      };

      const woocommerceFormatData = {
        user_login: email,
        user_password: password,
      };

      const { data } = await api(apiKeys.url).post(
        'user-login',
        woocommerceFormatData,
        apiHeader
      );

      dispatch({
        type: 'SIGN_IN_SUCCESS',
        payload: data,
      });

      navigation.goBack();

      return Alert.alert('Sucesso!', 'Você está logado.');
    } catch (error) {
      console.tron.log(`error`, error.message);
      return Alert.alert(
        'Erro',
        `Algo deu errado, verifique suas credenciais e tente novamente por favor!`
      );
    }
  }

  return (
    <Form
      type="signin"
      schema={schema}
      buttonText="ENTRAR"
      submit={handleSubmit}
      navigation={navigation}
      inputs={InputFactory(inputs)}
    />
  );
}

export default withNavigation(SignIn);
