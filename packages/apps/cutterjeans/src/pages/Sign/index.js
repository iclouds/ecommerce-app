import React, { useState, useEffect } from 'react';
import { StatusBar, View, Animated, Keyboard } from 'react-native';

import { FormHeader as Header } from 'components';

import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import { Container } from './styles';

const headerMenuItems = [
  { title: 'Entrar', type: 'signin' },
  { title: 'Cadastrar', type: 'signup' },
];

export default function Sign() {
  const [activeMenuItem, setActiveMenuItem] = useState('signin');
  const [logoSize] = useState({
    width: new Animated.Value(117),
    height: new Animated.Value(70),
  });

  useEffect(() => {
    Keyboard.addListener('keyboardWillHide', () => {
      Animated.parallel([
        Animated.timing(logoSize.width, {
          toValue: 117,
          duration: 300,
        }),
        Animated.timing(logoSize.height, {
          toValue: 70,
          duration: 300,
        }),
      ]).start();
    });

    Keyboard.addListener('keyboardWillShow', () => {
      Animated.parallel([
        Animated.timing(logoSize.width, {
          toValue: 80,
          duration: 300,
        }),
        Animated.timing(logoSize.height, {
          toValue: 50,
          duration: 300,
        }),
      ]).start();
    });
  }, []);

  function renderFormDynamically() {
    if (activeMenuItem === 'signin') return <SignIn />;

    if (activeMenuItem === 'signup') return <SignUp />;

    return <View />;
  }

  return (
    <>
      <StatusBar backgroundColor="#FFF" barStyle="dark-content" />

      <Container>
        <Header
          items={headerMenuItems}
          activeMenuItem={activeMenuItem}
          setActiveMenuItem={setActiveMenuItem}
        />

        {renderFormDynamically()}
      </Container>
    </>
  );
}

Sign.navigationOptions = {
  header: null,
};
