import styled from 'styled-components/native';

export const Logo = styled.Image.attrs({
  source: require('../../assets/img/logo.png'),
})`
  width: 45px;
  height: 100%;

  align-items: center;
  justify-content: center;

  resize-mode: contain;
`;
