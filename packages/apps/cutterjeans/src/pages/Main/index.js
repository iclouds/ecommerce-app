import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Platform } from 'react-native';
import { bindActionCreators } from 'redux';
import { withNavigation } from 'react-navigation';

import { metrics } from 'theme';
import { HeaderLeft } from 'components';
import { ListingProducts } from 'pages';

import { HeaderRight } from '../../components';
import { Creators as ProductsActions } from '../../store/ducks/products';

import { Logo } from './styles';

function Main({ navigation, products, getProductsRequest }) {
  const [page, setPage] = useState(1);

  useEffect(() => {
    getProductsRequest(page);

    setPage(page + 1);
  }, []);

  function fetchMoreProducts() {
    if (!products.isAllLoaded) {
      getProductsRequest(page);
      setPage(page + 1);
    }
  }

  return (
    <>
      <ListingProducts
        products={products.data}
        isLoaded={!products.loading}
        fetchMoreProducts={fetchMoreProducts}
      />
    </>
  );
}

Main.defaultProps = {
  products: [{}],
  getProductsRequest: () => {},
};

Main.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
  products: PropTypes.shape(),
  getProductsRequest: PropTypes.func,
};

Main.navigationOptions = {};

const mapStateToProps = state => ({
  products: state.products,
});

const mapDispatchToProps = dispatch => bindActionCreators(ProductsActions, dispatch);

export default {
  screen: connect(
    mapStateToProps,
    mapDispatchToProps,
  )(withNavigation(Main)),

  navigationOptions: ({ navigation }) => ({
    title: navigation.getParam('productName'),
    headerLayoutPreset: 'center',
    headerRight: (
      <HeaderRight
        iconNames={['cart-outline']}
        onPressIcons={[() => navigation.navigate('Cart')]}
      />
    ),
    headerTitle: <Logo />,
    headerLeft: <HeaderLeft cartItems={1} />,
    headerLeftContainerStyle: {
      marginLeft: Platform.OS !== 'android' ? 0 : metrics.baseContainer,
    },
  }),
};
