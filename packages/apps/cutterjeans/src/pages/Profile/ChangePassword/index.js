import React from 'react';
import * as Yup from 'yup';
import { useSelector } from 'react-redux';

import { api } from 'services';
import { Form } from 'components';
import { Alert } from 'react-native';
import { InputFactory } from 'functions';

import apiKeys from '../../../config/ApiKeys';

const schema = Yup.object().shape({
  password: Yup.string().required('Insira a senha atual!'),
  newPassword: Yup.string().required('Insira a nova senha'),
  confirmNewPassword: Yup.string().when('newPassword', (newPassword, field) =>
    newPassword
      ? field
          .required()
          .oneOf([Yup.ref('newPassword')], 'As senhas estão diferentes!')
      : field
  ),
});

const inputs = [
  {
    labelText: 'Senha atual',
    type: 'password',
    secureTextEntry: true,
    style: { marginBottom: 20 },
  },
  {
    labelText: 'Nova senha',
    type: 'newPassword',
    secureTextEntry: true,
  },
  {
    labelText: 'Confirmar senha',
    type: 'confirmNewPassword',
    secureTextEntry: true,
  },
];

export default function ChangePassword() {
  const { id, jwt } = useSelector(state => state.user.data);

  async function handleSubmit({ password, newPassword }) {
    const params = {
      user_id: id,
      user_password: password,
      user_new_password: newPassword,
    };

    try {
      const response = await api(apiKeys.url).post('change-password', params, {
        headers: {
          jwt,
        },
      });

      Alert.alert('Sucesso', 'Senha alterada.');
    } catch (error) {
      Alert.alert('Error', 'Verifique se a senha atual está correta!');
    }
  }

  return (
    <Form
      schema={schema}
      buttonText="SALVAR"
      submit={handleSubmit}
      inputs={InputFactory(inputs)}
    />
  );
}
