import styled from 'styled-components/native';
import { Platform, KeyboardAvoidingView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const DynamicContainer = Platform.OS === 'ios' ? KeyboardAwareScrollView : KeyboardAvoidingView;

export const Container = styled(DynamicContainer).attrs({
  contentContainerStyle: {
    justifyContent: 'center',
  },
  enableResetScrollToCoords: false,
})`
  margin: 73px 0 20px;
  padding: 0 33px;

  flex: 1;
`;
