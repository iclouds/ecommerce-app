import React, { useState } from 'react';
import { Platform } from 'react-native';

import { metrics } from 'theme';
import { HeaderLeft, HeaderRight, FormHeader as Header } from 'components';

import MyInfo from './MyInfo';
import ChangePassword from './ChangePassword';
import { Container } from './styles';

const headerMenuItems = [
  { title: 'Meus dados', type: 'myinfo' },
  { title: 'Alterar senha', type: 'changepassword' },
];

function Profile() {
  const [activeMenuItem, setActiveMenuItem] = useState('myinfo');

  return (
    <Container>
      <Header
        withoutLogo
        items={headerMenuItems}
        activeMenuItem={activeMenuItem}
        setActiveMenuItem={setActiveMenuItem}
      />

      {activeMenuItem === 'myinfo' && <MyInfo />}
      {activeMenuItem === 'changepassword' && <ChangePassword />}
    </Container>
  );
}

export default {
  screen: Profile,
  navigationOptions: ({ navigation }) => ({
    title: 'Minha conta',
    headerRight: (
      <HeaderRight
        iconNames={['home-outline', 'cart-outline']}
        onPressIcons={[() => navigation.navigate('Home'), () => navigation.navigate('Cart')]}
      />
    ),
    headerLeft: <HeaderLeft cartItems={1} />,
    headerLeftContainerStyle: {
      marginLeft: Platform.OS !== 'android' ? 0 : metrics.baseContainer,
    },
  }),
};
