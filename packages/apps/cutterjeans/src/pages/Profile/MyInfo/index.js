import React from 'react';
import * as Yup from 'yup';
import { Platform, Alert } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import { Form } from 'components';
import { InputFactory } from 'functions';
import { woocommerceApi } from 'services';

import apiKeys from '../../../config/ApiKeys';

const schema = Yup.object().shape({
  name: Yup.string().nullable(),
  lastname: Yup.string().nullable(),
  email: Yup.string().email('Email inválido!'),
  phone: Yup.string().nullable(),
});

export default function MyInfo() {
  const dispatch = useDispatch();
  const userInfo = useSelector(state => state.user.data);

  const inputs = [
    {
      labelText: 'Nome',
      type: 'name',
      autoCorrect: true,
      autoCompleteType: 'name',
      returnKeyType: 'next',
      value: userInfo.firstname,
    },
    {
      labelText: 'Sobrenome',
      type: 'lastname',
      autoCorrect: true,
      autoCompleteType: 'name',
      returnKeyType: 'next',
      value: userInfo.lastname,
    },
    {
      labelText: 'Email',
      type: 'email',
      keyboardType: 'email-address',
      autoCompleteType: 'email',
      autoCorrect: true,
      returnKeyType: 'next',
      value: userInfo.email,
    },
    {
      labelText: 'Telefone',
      type: 'phone',
      autoCorrect: true,
      autoCompleteType: 'tel',
      returnKeyType: 'done',
      keyboardType:
        Platform.OS === 'ios' ? 'numbers-and-punctuation' : 'phone-pad',
      value: userInfo.phone,
    },
  ];

  async function handleSubmit({ name, lastname, email, phone }) {
    const userData = {
      id: userInfo.id,
      first_name: name,
      last_name: lastname,
      email,
      billing: {
        first_name: name,
        last_name: lastname,
        phone,
      },
    };

    try {
      const {
        first_name,
        last_name,
        email,
        billing,
      } = await woocommerceApi.updateUser(
        apiKeys.username,
        apiKeys.password,
        apiKeys.urlSdk,
        userData
      );

      const updatedUserInfo = {
        firstname: first_name,
        lastname: last_name,
        email,
        phone: billing.phone,
        nasc: billing.birthdate,
        address: {
          cep: billing.postcode,
          street: billing.address_1,
          neighborhood: billing.neighborhood,
          city: billing.city,
          state: billing.state,
          number: billing.number,
          complement: billing.address_2,
        },
      };

      dispatch({ type: 'EDIT_USER_INFO', payload: updatedUserInfo });

      Alert.alert('Sucesso!', 'Dados atualizados!');
    } catch (error) {
      Alert.alert('Error', 'Algo deu errado, tente novamente!');
    }
  }

  return (
    <Form
      schema={schema}
      buttonText="SALVAR"
      submit={handleSubmit}
      inputs={InputFactory(inputs)}
    />
  );
}
