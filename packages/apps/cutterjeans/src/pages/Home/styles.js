import styled from 'styled-components/native';
import { Platform } from 'react-native';

import { metrics, fonts } from 'theme';
import { ListingProducts as ProductsList } from 'pages';

export const Container = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
})``;

export const Highlights = styled.View`
  margin: ${metrics.baseContainer}px;
`;

export const Title = styled(fonts.size_6x)`
  margin-left: 4px;
  margin-top: 28px;
  margin-bottom: ${metrics.baseMargin_1x}px;

  text-transform: uppercase;
  font-family: ${Platform.OS === 'android' ? 'yumin' : 'YuMincho-Regular'};
`;

export const Logo = styled.Image.attrs({
  source: require('../../assets/img/logo.png'),
})`
  width: 45px;
  height: 100%;

  align-items: center;
  justify-content: center;

  resize-mode: contain;
`;

export const ListingProducts = styled(ProductsList)`
  padding: 0;
`;
