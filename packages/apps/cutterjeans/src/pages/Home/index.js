import React, { useState, useEffect } from 'react';
import { Platform } from 'react-native';
import { withNavigation } from 'react-navigation';
import { useDispatch, useSelector } from 'react-redux';

import { metrics } from 'theme';
import {
  FeaturedCategoriesCarousel,
  CategoryCarousel,
  ContactFooter,
  HeaderLeft,
} from 'components';

import { Creators as ChatActions } from '../../store/ducks/chat';
import { Creators as AdminActions } from '../../store/ducks/admin';
import { Creators as ProductsActions } from '../../store/ducks/products';
import { Creators as CategoriesActions } from '../../store/ducks/categories';
import { Creators as FeaturedProductsActions } from '../../store/ducks/featuredProducts';

import { Container, Title, Highlights, Logo, ListingProducts } from './styles';

import { HeaderRight } from '../../components';

import { woocommerceApi } from 'services';
import apiKeys from '../../config/ApiKeys';

function Home({ navigation }) {
  const dispatch = useDispatch();

  const categories = useSelector(state => state.categories);
  const adminInfo = useSelector(state => state.admin.data);
  const featuredProducts = useSelector(state => state.featuredProducts);
  const isShowingBallon = useSelector(state => state.chat.isShowingBallon);

  const [page, setPage] = useState(1);
  const [featuredCategories, setFeaturedCategories] = useState([]);
  const [footerCategories, setFooterCategories] = useState([]);

  async function handleFeaturedCategories() {
    const categories = await woocommerceApi.getAllFeaturedCategories(
      apiKeys.username,
      apiKeys.password,
      apiKeys.urlSdk
    );

    setFeaturedCategories(categories);
  }

  function* handleFooterCategories(){
    const categories = yield woocommerceApi.getAllFooterCategories(
      apiKeys.username,
      apiKeys.password,
      apiKeys.urlSdk,
    )

    setFooterCategories(categories);
  }

  useEffect(() => {
    dispatch(ProductsActions.getProductsRequest(page));
    dispatch(FeaturedProductsActions.getFeaturedProductsRequest(page));
    dispatch(CategoriesActions.getCategoriesRequest());
    dispatch(AdminActions.requestInfo());

    navigation.addListener('willBlur', () => dispatch({ type: 'HIDE_CHAT' }));

    handleFeaturedCategories();
    handleFooterCategories();
  }, []);

  function handleShowingBalloon(event) {
    const distanceFromTop = event.y;

    if (distanceFromTop >= 550) {
      return dispatch({ type: 'HIDE_BALLON' });
    }

    if (!isShowingBallon) {
      return dispatch({ type: 'HIDE_CHAT' });
    }
  }

  return (
    <Container
      onScroll={event => handleShowingBalloon(event.nativeEvent.contentOffset)}
    >
      <FeaturedCategoriesCarousel
        items={featuredCategories}
        isLoaded={featuredCategories.length > 0}
        showDetailsOnPress={true}
      />

      <Highlights>
        <Title>Destaques</Title>

        <ListingProducts
          products={featuredProducts.data.slice(0, 4)}
          isLoaded={!featuredProducts.loading}
        />
      </Highlights>

      <CategoryCarousel
        style={{ marginBottom: 40 }}
        items={footerCategories}
        isLoaded={!categories.loading}
      />

      <ContactFooter
        adminInfo={adminInfo}
        onPress={() => dispatch(ChatActions.showChat())}
      />
    </Container>
  );
}

export default {
  screen: withNavigation(Home),
  navigationOptions: ({ navigation }) => ({
    title: navigation.getParam('productName'),
    headerRight: (
      <HeaderRight
        iconNames={['cart-outline']}
        onPressIcons={[() => navigation.navigate('Cart')]}
      />
    ),
    headerTitle: <Logo />,
    headerLeft: <HeaderLeft cartItems={1} />,
    headerLeftContainerStyle: {
      marginLeft: Platform.OS !== 'android' ? 0 : metrics.baseContainer,
    },
  }),
};
