/* eslint-disable react/prop-types */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Text, Platform } from 'react-native';
import { withNavigation } from 'react-navigation';
import { useSelector, useDispatch } from 'react-redux';

import { api } from 'services';
import { colors } from 'theme';
import { HeaderRight } from 'components';
import { Checkout as CheckoutPage } from 'pages';
import { RemoveSpecialCharacters } from 'functions';

import themeColors from '../../styles/colors';
import apiKeys from '../../config/ApiKeys';

function Checkout({ navigation }) {
  const dispatch = useDispatch();

  const userInfo = useSelector(state => state.user.data);
  const cart = useSelector(state => state.cart.data);

  const [isLoading, setIsLoading] = useState(false);

  function changeFromDashToSlash(word) {
    return word.replace(/-/g, '/');
  }

  function formatDataToWoocommerce(data) {
    const cartItems = cart.map(item => ({
      product_id: item.id,
      quantity: item.amount,
    }));

    const woocommerceDataStructure = {
      meta_data: [
        {
          key: '_billing_cpf',
          value: RemoveSpecialCharacters(data.cpf),
        },
        {
          key: '_billing_phone',
          value: RemoveSpecialCharacters(data.phone),
        },
        {
          key: '_billing_birthdate',
          value: changeFromDashToSlash(data.nasc),
        },
        {
          key: '_billing_number',
          value: data.address.number,
        },
        {
          key: '_billing_neighborhood',
          value: data.address.neighborhood,
        },
      ],
      billing: {
        first_name: data.firstname,
        last_name: data.lastname,
        address_1: data.address.street,
        address_2: data.address.complement,
        billing_cpf: RemoveSpecialCharacters(data.cpf),
        city: data.address.city,
        state: data.address.state,
        postcode: RemoveSpecialCharacters(data.address.cep),
        country: 'BR',
        email: data.email.toLowerCase(),
      },
      line_items: [...cartItems],
      shipping_lines: [
        {
          method_id: 'flat_rate',
          method_title: 'Flat Rate',
          total: String(data.payment.frete.price),
        },
      ],
    };

    return woocommerceDataStructure;
  }

  function paymentInformations(data, orderId) {
    if (data.payment.boleto) {
      return {
        payment_method: 'banking_ticket',
        order_id: orderId,
      };
    }

    function concatNumbers(numberOne, numberTwo) {
      return String(numberOne) + String(numberTwo);
    }

    return {
      payment_method: 'credit_card',
      order_id: orderId,
      card_number: RemoveSpecialCharacters(data.payment.credit_card.number),
      card_cvv: data.payment.credit_card.cvv,
      card_expiration_date: concatNumbers(
        data.payment.credit_card.month,
        data.payment.credit_card.year
      ),
      card_holder_name: data.payment.credit_card.name,
      installments: data.payment.credit_card.installments,
    };
  }

  async function handleBuySubmit(allPurchaseInformation) {
    setIsLoading(true);

    try {
      const woocommercePurchaseInformations = formatDataToWoocommerce(
        allPurchaseInformation
      );

      const apiHeader = {
        auth: {
          username: apiKeys.username,
          password: apiKeys.password,
        },
        headers: {
          jwt: userInfo.jwt,
        },
      };

      const orderResponse = await api(apiKeys.url).post(
        'orders',
        woocommercePurchaseInformations,
        apiHeader
      );

      const purchaseId = orderResponse.data.id;

      const paymentInformation = paymentInformations(
        allPurchaseInformation,
        purchaseId
      );

      const paymentResponse = await api(apiKeys.url).post(
        'payment',
        paymentInformation,
        apiHeader
      );

      if (allPurchaseInformation.payment.boleto === true)
        return navigation.navigate('Feedback', {
          status: 'success',
          purchaseId,
        });

      if (paymentResponse.data.result !== 'success')
        return navigation.navigate('Feedback', {
          status: 'failure',
          message: 'Falha nos dados do cartão de crédito',
        });

      return navigation.navigate('Feedback', { status: 'success', purchaseId });
    } catch (error) {
      navigation.navigate('Feedback', { status: 'failure' });
    } finally {
      setIsLoading(false);
    }
  }

  function handleAddressChange(data) {
    return dispatch({ type: 'EDIT_ADDRESS_USER_INFO', payload: data });
  }

  function handleInfoChange(data) {
    return dispatch({ type: 'EDIT_USER_INFO', payload: data });
  }

  return (
    <CheckoutPage
      cart={cart}
      apiKeys={apiKeys}
      colors={themeColors}
      isLoading={isLoading}
      baseurl={apiKeys.url}
      initialReduxUserInfo={userInfo}
      handleBuySubmit={handleBuySubmit}
      handleInfoChange={handleInfoChange}
      handleAddressChange={handleAddressChange}
    />
  );
}

Checkout.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

export default {
  screen: withNavigation(Checkout),
  navigationOptions: ({ navigation }) => ({
    headerLeft: null,
    headerTitle: (
      <Text
        style={{
          fontSize: 19,
          fontWeight: 'bold',
          color: colors.white,
          marginLeft: Platform.OS === 'android' ? 20 : 0,
        }}
      >
        Checkout
      </Text>
    ),
    headerRight: (
      <HeaderRight
        fontSize={30}
        cartItems={1}
        iconNames={['close']}
        onPressIcons={[() => navigation.goBack()]}
      />
    ),
  }),
};
