import React from 'react';
import PropTypes from 'prop-types';
import { connect, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withNavigation } from 'react-navigation';

import { ProductDetail } from 'pages';
import { FilterSpecificProduct } from 'functions';

import colors from '../../styles/colors';
import { HeaderRight } from '../../components';
import { Creators as CartActions } from '../../store/ducks/cart';
import { Creators as ProductsActions } from '../../store/ducks/products';
import apiKeys from '../../config/ApiKeys';


function Product({
  navigation, products, addToCart, editProduct, cart,
}) {
  const pageProductId = navigation.getParam('productId');

  const featuredProducts = useSelector(state => state.featuredProducts);
  const productsToFilter = products.data.length > 0 ? products.data : featuredProducts.data;

  const pageProductData = FilterSpecificProduct(productsToFilter, pageProductId);

  return (
    <ProductDetail
      colors={colors}
      addToCart={addToCart}
      product={pageProductData}
      editProduct={editProduct}
      getProductsEdit={() => {}}
      isLoaded={!!pageProductData}
      apiKeys={apiKeys}
      cart={cart}
    />
  );
}

Product.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
  products: PropTypes.shape().isRequired,
  addToCart: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  products: state.products,
  cart: state.cart,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    addToCart: CartActions.addToCart,
    editProduct: ProductsActions.getProductsEdit,
  },
  dispatch,
);

export default {
  screen: connect(
    mapStateToProps,
    mapDispatchToProps,
  )(withNavigation(Product)),

  navigationOptions: ({ navigation }) => ({
    title: navigation.getParam('productName'),
    headerRight: (
      <HeaderRight
        iconNames={['home-outline', 'cart-outline']}
        onPressIcons={[() => navigation.navigate('Home'), () => navigation.navigate('Cart')]}
      />
    ),
  }),
};
