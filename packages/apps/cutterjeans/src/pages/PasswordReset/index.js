import React from 'react';
import * as Yup from 'yup';
import { Alert } from 'react-native';

import { api } from 'services';
import { InputFactory } from 'functions';
import { Form, FormHeader as Header } from 'components';

import apiKeys from '../../config/ApiKeys';
import { Container } from './styles';

const schema = Yup.object().shape({
  email: Yup.string()
    .email('Email inválido!')
    .required('Insira um Email!'),
});

const inputs = [
  {
    type: 'email',
    autoCorrect: true,
    labelText: 'Email',
    returnKeyType: 'next',
    autoCompleteType: 'email',
    keyboardType: 'email-address',
  },
];

const headerMenuItems = [
  {
    type: 'passwordReset',
    title: 'Recuperar senha',
    description:
      'Digite seu endereço de e-mail. Você receberá um link por e-mail para criar uma nova senha.',
  },
];

async function handleSubmit({ email }) {
  try {
    await api(apiKeys.url).get('reset-password', {
      params: {
        user_email: email,
      }
    });

    Alert.alert(
      'Sucesso',
      'Verifique no email informado as intruções para redifinição da senha!'
    );
  } catch (error) {
    Alert.alert('Error', JSON.stringify(error.response.data.message));
  }
}

function PasswordReset() {
  return (
    <Container>
      <Header items={headerMenuItems} activeMenuItem="passwordReset" />

      <Form
        schema={schema}
        buttonText="ENVIAR"
        type="passwordReset"
        submit={handleSubmit}
        inputs={InputFactory(inputs)}
      />
    </Container>
  );
}

PasswordReset.navigationOptions = {
  headerStyle: {
    backgroundColor: '#FFF',
  },
  headerTintColor: '#333',
  headerTintStyle: { color: '#333' },
};

export default PasswordReset;
