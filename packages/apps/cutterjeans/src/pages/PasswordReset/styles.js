import styled from 'styled-components/native';
import { Animated, Platform } from 'react-native';

export const Container = styled.KeyboardAvoidingView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  padding: 0 20px;
  margin-top: -100px;

  flex: 1;
  justify-content: center;
`;

export const Menu = styled.View`
  flex-direction: row;
`;

export const AnimatedLogo = styled(Animated.View)`
  height: 70px;
  width: 117px;
  margin-bottom: 56px;

  overflow: visible;
`;

export const Logo = styled.Image.attrs({
  source: require('../../assets/img/logo-white.png'),
})`
  width: 100%;
  height: 100%;

  overflow: visible;
`;
