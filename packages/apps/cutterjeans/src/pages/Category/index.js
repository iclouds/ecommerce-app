import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withNavigation } from 'react-navigation';

import { ListingProducts } from 'pages';
import { FilterProductByCategory } from 'functions';

import { Creators as ProductsActions } from '../../store/ducks/products';

function Category({ navigation, products }) {
  const pageCategory = navigation.getParam('category');

  return (
    <ListingProducts
      isLoaded={!products.loading}
      screenProps={{ teste: true }}
      products={FilterProductByCategory(products.data, pageCategory)}
    />
  );
}

Category.defaultProps = {
  products: [{}],
};

Category.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
  products: PropTypes.shape(),
};

const mapStateToProps = state => ({
  products: state.products,
});

const mapDispatchToProps = dispatch => bindActionCreators(ProductsActions, dispatch);

export default {
  screen: connect(
    mapStateToProps,
    mapDispatchToProps,
  )(withNavigation(Category)),
  navigationOptions: ({ navigation }) => ({
    title: navigation.getParam('category'),
  }),
};
