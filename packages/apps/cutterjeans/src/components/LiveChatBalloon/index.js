import React from 'react';
import { useSelector } from 'react-redux';

import { LiveChatBalloon as Balloon } from 'components';

export default function LiveChatBalloon({ onPress, isShowing }) {
  const { avatar } = useSelector(state => state.admin.data);
  const unseenMessages = useSelector(state => state.messages.unseenMessages);
  const { isOnHome } = useSelector(state => state.chat);

  return (
    <Balloon
      avatar={avatar}
      onPress={onPress}
      isOnHome={isOnHome}
      isShowing={isShowing}
      notifications={unseenMessages}
    />
  );
}
