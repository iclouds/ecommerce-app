import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

import { Messenger } from 'components';

import { baseApi } from '../../services/api';

export default function Messager({ close, isVisible }) {
  const dispatch = useDispatch();

  const user = useSelector(state => state.user.data);
  const messages = useSelector(store => store.messages.data);
  const adminInfo = useSelector(store => store.admin.data);

  const dispatchs = {
    dispatchLoadMessages: data =>
      dispatch({ type: 'LOAD_MESSAGES', payload: data }),

    dispatchNewUser: data => dispatch({ type: 'ADD_USER_INFO', payload: data }),
    dispatchNewMessage: message =>
      dispatch({ type: 'ADD_MESSAGE', payload: message }),

    dispatchUnseenMessage: () => dispatch({ type: 'ADD_UNSEEN_MESSAGE' }),
    dispatchRemoveUnseenMessages: () =>
      dispatch({ type: 'REMOVE_UNSEEN_MESSAGES' }),
  };

  return (
    <Messenger
      user={user}
      close={close}
      baseApi={baseApi}
      messages={messages}
      adminInfo={adminInfo}
      isVisible={isVisible}
      {...dispatchs}
    />
  );
}

Messager.propTypes = {
  close: PropTypes.func.isRequired,
  isVisible: PropTypes.bool.isRequired,
};
