import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { HeaderRight as RootHeaderRight } from 'components';

function HeaderRight(props) {
  const { cart } = props;
  return (
    <RootHeaderRight
      {...props}
      cartItems={cart.length}
    />
  );
}

HeaderRight.propTypes = {
  cart: PropTypes.arrayOf(PropTypes.shape).isRequired,
};

const mapStateToProps = state => ({
  cart: state.cart.data,
});

export default connect(mapStateToProps)(HeaderRight);
