import React, { useEffect } from 'react';
import { StatusBar, Linking } from 'react-native';
import OneSignal from 'react-native-onesignal';
import { useSelector, useDispatch } from 'react-redux';

import Routes from './routes';
import colors from './styles/colors';
import api from './services/api';
import { Messager, LiveChatBalloon } from './components';
import { Creators as ChatActions } from './store/ducks/chat';

export default function App() {
  if(__DEV__) {
    console.disableYellowBox = true;
  }

  const dispatch = useDispatch();

  const { isShowingChat, isShowingBallon } = useSelector(state => state.chat);
  const { email, is_admin } = useSelector(state => state.user.data);

  useEffect(() => {
    async function handleAdminNotifications({ email, onesignal }) {
      try {
        await api.patch('admin/email', { email, onesignal });
      } catch (err) {
        // alert(
        //   'Algo deu errado ao verificaser se o usuário é admin',
        //   err.message
        // );
      }
    }

    function onIds({ userId: onesignal }) {
      if (email !== '' && is_admin) {
        handleAdminNotifications({ email, onesignal });
      }
    }

    function onOpened() {
      Linking.openURL('https://cutterjeans.iclouds.com.br');
    }

    OneSignal.init('bcbb95a9-f54d-4989-97a6-bacd5efef94c');
    OneSignal.inFocusDisplaying(0);

    OneSignal.addEventListener('ids', onIds);
    OneSignal.addEventListener('opened', onOpened);

    return () => {
      OneSignal.removeEventListener('ids', onIds);
      OneSignal.removeEventListener('opened', onOpened);
    };
  }, []);

  return (
    <>
      <StatusBar
        translucent={false}
        barStyle="light-content"
        backgroundColor={isShowingChat ? colors.secondary : colors.primary}
      />

      <Routes />

      <Messager
        isVisible={isShowingChat}
        close={() => dispatch(ChatActions.hideChat())}
      />

      <LiveChatBalloon
        isShowing={isShowingBallon}
        onPress={() => dispatch(ChatActions.showChat())}
      />
    </>
  );
}
