import { woocommerceApi } from 'services';
import { put } from 'redux-saga/effects';

import apiKeys from '../../config/ApiKeys';

import { Creators as ProductsActions } from '../ducks/products';
import { Creators as FeaturedProductsActions } from '../ducks/featuredProducts';

export function* fetchProducts({ page, per_page }) {
  const NUMBER_OF_PRODUCTS = 40;

  try {
    const response = yield woocommerceApi.getAllProducts(
      apiKeys.username,
      apiKeys.password,
      apiKeys.urlSdk,
      {
        page,
        per_page: per_page || NUMBER_OF_PRODUCTS,
        offset: NUMBER_OF_PRODUCTS * (page - 1),
      }
    );

    const products = response.filter((product) => {
      if (product.status === 'draft') return false

      return true;
    });

    yield put(ProductsActions.getProductsSuccess(products));
  } catch (error) {
    yield put(ProductsActions.getProductsFailure('Erro ao buscar os produtos'));
  }
}

export function* fetchFeaturedProducts({ page }) {
  const NUMBER_OF_PRODUCTS = 40;

  try {
    const response = yield woocommerceApi.getAllProducts(
      apiKeys.username,
      apiKeys.password,
      apiKeys.urlSdk,
      {
        page,
        per_page: NUMBER_OF_PRODUCTS,
        offset: NUMBER_OF_PRODUCTS * (page - 1),
        featured: true,
      }
    );

    const products = response.filter((product) => {
      if (product.status === 'draft') return false

      return true;
    });

    yield put(FeaturedProductsActions.getFeaturedProductsSuccess(products));
  } catch (error) {
    yield put(
      FeaturedProductsActions.getFeaturedProductsFailure(
        'Erro ao buscar os produtos'
      )
    );
  }
}
