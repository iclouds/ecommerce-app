import { call, put } from 'redux-saga/effects';
import api from '../../services/api';

import { Creators as AdminActions } from '../ducks/admin';

export function* fetchUserInfo() {
  try {
    const {
      data: { name, role, avatar },
    } = yield call(api.get, '/admin/current');

    yield put({ type: 'SUCCESS_INFO', payload: { name, role, avatar } });
  } catch (error) {
    yield put({ type: 'ERROR_INFO' });
  }
}
