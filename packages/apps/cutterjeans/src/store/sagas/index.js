import { all, takeLatest } from 'redux-saga/effects';

import { Types as ProductsTypes } from '../ducks/products';
import { Types as FeaturedProductsTypes } from '../ducks/featuredProducts';
import { Types as CategoriesTypes } from '../ducks/categories';
import { Types as AdminTypes } from '../ducks/admin';

import { fetchProducts, fetchFeaturedProducts } from './products';
import { fetchCategories } from './categories';
import { fetchUserInfo } from './adminInfo';

export default function* rootSaga() {
  return yield all([
    takeLatest(AdminTypes.REQUEST_INFO, fetchUserInfo),
    takeLatest(ProductsTypes.GET_PRODUCTS_REQUEST, fetchProducts),
    takeLatest(
      FeaturedProductsTypes.GET_FEATURED_PRODUCTS_REQUEST,
      fetchFeaturedProducts
    ),
    takeLatest(CategoriesTypes.GET_CATEGORIES_REQUEST, fetchCategories),
  ]);
}
