import { woocommerceApi } from 'services';
import { put } from 'redux-saga/effects';

import apiKeys from '../../config/ApiKeys';

import { Creators as CategoriesActions } from '../ducks/categories';

export function* fetchCategories() {
  try {
    const response = yield woocommerceApi.getAllCategories(
      apiKeys.username,
      apiKeys.password,
      apiKeys.urlSdk
    );

    yield put(CategoriesActions.getCategoriesSuccess(response));
  } catch (error) {
    yield put(
      CategoriesActions.getCategoriesFailure('Erro ao buscar os produtos')
    );
  }
}
