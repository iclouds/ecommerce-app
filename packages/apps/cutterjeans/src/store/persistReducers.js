import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';

const persistConfig = {
  storage: AsyncStorage,
  key: 'cutterjeans',
  whitelist: ['messages', 'user'],
};

export default reducers => persistReducer(persistConfig, reducers);
