import { combineReducers } from 'redux';

import cart from './cart';
import chat from './chat';
import user from './user';
import admin from './admin';
import products from './products';
import messages from './messages';
import categories from './categories';
import featuredProducts from './featuredProducts';

const reducers = combineReducers({
  cart,
  chat,
  user,
  admin,
  products,
  messages,
  categories,
  featuredProducts,
});

export default reducers;
