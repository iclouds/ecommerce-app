import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  showChat: [],
  hideChat: [],
  hideBallon: [],
});

const INITIAL_STATE = {
  isShowingChat: false,
  isShowingBallon: true,
  isOnHome: true,
};

const show = (state = INITIAL_STATE, actions) => ({
  ...state,
  isShowingChat: true,
  isShowingBallon: false,
});

const hide = (state = INITIAL_STATE, actions) => ({
  isOnHome: false,
  isShowingChat: false,
  isShowingBallon: true,
});

const hideBallon = (state = INITIAL_STATE, actions) => ({
  ...state,
  isShowingBallon: false,
});

export default createReducer(INITIAL_STATE, {
  [Types.SHOW_CHAT]: show,
  [Types.HIDE_CHAT]: hide,
  [Types.HIDE_BALLON]: hideBallon,
});
