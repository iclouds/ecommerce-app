/* eslint-disable no-unused-vars */
import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  getCategoriesRequest: [],
  getCategoriesSuccess: ['data'],
  getCategoriesFailure: ['error'],
});

const INITIAL_STATE = {
  data: [],
  loading: false,
  error: null,
};

const request = (state = INITIAL_STATE, action) => ({
  ...state,
  loading: true,
});

const success = (state = INITIAL_STATE, action) => ({
  data: action.data,
  loading: false,
  error: null,
});

const failure = (state = INITIAL_STATE, action) => ({
  ...state,
  loading: false,
  error: action.error,
});

export default createReducer(INITIAL_STATE, {
  [Types.GET_CATEGORIES_REQUEST]: request,
  [Types.GET_CATEGORIES_SUCCESS]: success,
  [Types.GET_CATEGORIES_FAILURE]: failure,
});
