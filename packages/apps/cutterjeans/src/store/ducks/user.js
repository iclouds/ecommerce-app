import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  addUserInfo: ['data'],
  editUserInfo: ['data'],
  editAddressUserInfo: ['data'],

  signInRequest: [],
  signInSuccess: ['data'],
  signInError: ['data'],

  signUpRequest: ['data'],
  signUpSuccess: ['data'],
  signUpError: ['data'],

  signOut: [],
});

const INITIAL_STATE = {
  data: {
    firstname: '',
    lastname: '',
    email: '',
    phone: '',
    cpf: '',
    nasc: '',
    address: {
      cep: '',
      street: '',
      neighborhood: '',
      city: '',
      state: '',
      number: '',
      complement: '',
    },
  },
  isLoggedIn: false,
  isLoading: false,
  error: null,
};

const add = (state = INITIAL_STATE, actions) => ({
  ...state,
  data: { ...state.data, ...actions.payload },
});

const edit = (state = INITIAL_STATE, action) => ({
  ...state,
  data: {
    ...state.data,
    ...action.payload,
  },
});

const editAddress = (state = INITIAL_STATE, action) => ({
  ...state,
  data: {
    ...state.data,
    address: { ...state.data.address, ...action.payload },
  },
});

const request = (state = INITIAL_STATE, action) => ({
  ...state,
  isLoading: true,
});

const success = (state = INITIAL_STATE, action) => ({
  data: { ...state.data, ...action.payload },
  isLoading: false,
  error: false,
  isLoggedIn: true,
});

const error = (state = INITIAL_STATE, action) => ({
  ...state,
  isLoading: false,
  error: action.payload.error,
});

const signOut = (state = INITIAL_STATE, action) => INITIAL_STATE;

export default createReducer(INITIAL_STATE, {
  [Types.ADD_USER_INFO]: add,
  [Types.EDIT_USER_INFO]: edit,
  [Types.EDIT_ADDRESS_USER_INFO]: editAddress,

  [Types.SIGN_IN_REQUEST]: request,
  [Types.SIGN_IN_SUCCESS]: success,
  [Types.SIGN_IN_ERROR]: error,

  [Types.SIGN_UP_REQUEST]: request,
  [Types.SIGN_UP_SUCCESS]: success,
  [Types.SIGN_UP_ERROR]: error,

  [Types.SIGN_OUT]: signOut,
});
