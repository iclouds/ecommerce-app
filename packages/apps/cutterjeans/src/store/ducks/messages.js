import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  addMessage: ['data'],
  loadMessages: ['data'],
  addUnseenMessage: [],
  removeUnseenMessages: [],
});

const INITIAL_STATE = {
  data: [],
  unseenMessages: 0,
};

const add = (state = INITIAL_STATE, action) => ({
  ...state,
  data: [action.payload, ...state.data],
});

const load = (state = INITIAL_STATE, action) => ({
  unseenMessages: action.payload.unreadMessages,
  data: action.payload.previousMessages.reverse(),
});

const addUnseen = (state = INITIAL_STATE) => ({
  ...state,
  unseenMessages: state.unseenMessages + 1,
});

const removeUnseen = (state = INITIAL_STATE) => ({
  ...state,
  unseenMessages: 0,
});

export default createReducer(INITIAL_STATE, {
  [Types.ADD_MESSAGE]: add,
  [Types.LOAD_MESSAGES]: load,
  [Types.ADD_UNSEEN_MESSAGE]: addUnseen,
  [Types.REMOVE_UNSEEN_MESSAGES]: removeUnseen,
});
