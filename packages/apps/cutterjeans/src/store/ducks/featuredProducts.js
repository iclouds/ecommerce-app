import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  getFeaturedProductsRequest: ['page'],
  getFeaturedProductsEdit: ['data'],
  getFeaturedProductsSuccess: ['data'],
  getFeaturedProductsFailure: ['error'],
});

const INITIAL_STATE = {
  data: [],
  loading: false,
  error: null,
};

const request = (state = INITIAL_STATE, action) => ({
  ...state,
  page: action.page,
  featured: true,
  loading: true,
});

const edit = (state = INITIAL_STATE, action) => ({
  ...state,
  data: [
    ...state.data.map((product) => {
      if (product.id !== action.data.id) return product;

      function sum() {
        const newStockValue = parseInt(product.stock_quantity, 10) + parseInt(action.data.amount, 10);

        return newStockValue.toString();
      }

      function subtraction() {
        const newStockValue = parseInt(product.stock_quantity, 10) - parseInt(action.data.amount, 10);

        return product.stock_quantity > 0 && newStockValue.toString();
      }

      if (action.data.type === 'sum') return { ...product, stock_quantity: sum() };

      return { ...product, stock_quantity: subtraction() };
    }),
  ],
});

const success = (state = INITIAL_STATE, action) => ({
  data: [...state.data, ...action.data],
  isAllLoaded: (action.data.length !== 40),
  loading: false,
  error: null,
});

const failure = (state = INITIAL_STATE, action) => ({
  ...state,
  loading: false,
  error: action.error,
});

export default createReducer(INITIAL_STATE, {
  [Types.GET_FEATURED_PRODUCTS_EDIT]: edit,
  [Types.GET_FEATURED_PRODUCTS_REQUEST]: request,
  [Types.GET_FEATURED_PRODUCTS_SUCCESS]: success,
  [Types.GET_FEATURED_PRODUCTS_FAILURE]: failure,
});
