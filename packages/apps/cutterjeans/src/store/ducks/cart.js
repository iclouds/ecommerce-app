/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  addToCart: ['data'],
  removeFromCart: ['data'],
  cleanCart: [],
});

const INITIAL_STATE = {
  data: [],
};

const add = (state = INITIAL_STATE, action) => {
  if (state.data.length === 0) {
    return {
      data: [action.data],
    };
  }

  function haveAddedYet() {
    const itemsOnCart = state.data.filter(product => product.id === action.data.id);

    return itemsOnCart.length !== 0;
  }

  function addNewItem() {
    return [
      ...state.data.filter((item) => {
        if (
          item.id !== action.data.id
          || item.size !== action.data.size
          || item.color !== action.data.color
        ) return item;
      }),
      action.data,
    ];
  }

  function addedCartItems() {
    let haveDifferentAttributes = false;

    const allItems = [
      ...state.data.map((item) => {
        if (item.id !== action.data.id) {
          haveDifferentAttributes = false;

          return item;
        }

        if (item.size !== action.data.size || item.color !== action.data.color) {
          haveDifferentAttributes = true;

          return item;
        }

        haveDifferentAttributes = false;

        return {
          ...item,
          amount: (parseInt(item.amount, 10) + parseInt(action.data.amount, 10)).toString(),
        };
      }),
    ];

    function newItem() {
      if (haveDifferentAttributes) return allItems.push(action.data);

      return false;
    }

    newItem();

    return allItems;
  }

  function CartItems() {
    if (haveAddedYet()) return addedCartItems();

    return addNewItem();
  }

  return {
    data: CartItems(),
  };
};

const remove = (state = INITIAL_STATE, action) => ({
  data: [...state.data.filter(item => item.cartId !== action.data)],
});

const clean = (state = INITIAL_STATE, action) => ({
  data: [],
});

export default createReducer(INITIAL_STATE, {
  [Types.ADD_TO_CART]: add,
  [Types.REMOVE_FROM_CART]: remove,
  [Types.CLEAN_CART]: clean,
});
