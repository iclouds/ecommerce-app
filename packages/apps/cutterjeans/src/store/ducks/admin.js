import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  requestInfo: [],
  successInfo: ['data'],
  errorInfo: [],
});

const INITIAL_STATE = {
  data: {
    name: '',
    role: '',
    avatar: 'https://cutterjeans.herokuapp.com/files',
  },
  isLoading: false,
  error: false,
};

const request = (state = INITIAL_STATE, action) => ({
  ...state,
  isLoading: true,
  error: false,
});

const success = (state = INITIAL_STATE, action) => ({
  data: action.payload,
  isLoading: false,
  error: false,
});

const error = (state = INITIAL_STATE, action) => ({
  data: {
    name: 'Vendedor',
    role: 'Gerente de vendas',
    avatar:
      'https://cutterjeans.herokuapp.com/files',
  },
  isLoading: false,
  error: true,
});

export default createReducer(INITIAL_STATE, {
  [Types.REQUEST_INFO]: request,
  [Types.SUCCESS_INFO]: success,
  [Types.ERROR_INFO]: error,
});
