import React, { useEffect } from 'react';
import { YellowBox } from 'react-native';
import { Provider } from 'react-redux';
import CodePush from 'react-native-code-push';
import { ThemeProvider } from 'styled-components';
import SplashScreen from 'react-native-splash-screen';
import { PersistGate } from 'redux-persist/integration/react';

import './config/ReactotronConfig';

import App from './App';
import colors from './styles/colors';
import { store, persistor } from './store';

function Main() {
  YellowBox.ignoreWarnings(['Warning: Async Storage has been extracted from react-native core']);
  YellowBox.ignoreWarnings(['Require cycle']);

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <ThemeProvider theme={colors}>
            <App />
          </ThemeProvider>
        </PersistGate>
      </Provider>
    </>
  );
}

export default CodePush({
  checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME,
})(Main);
