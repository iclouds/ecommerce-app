import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import { metrics, colors as themeColors } from 'theme';

import Cart from '../pages/Cart';
import Checkout from '../pages/Checkout';
import Product from '../pages/Product';
import Category from '../pages/Category';
import Feedback from '../pages/Feedback';
import Home from '../pages/Home';
import Sign from '../pages/Sign';
import PasswordReset from '../pages/PasswordReset';
import Profile from '../pages/Profile';

const Config = {
  defaultNavigationOptions: {
    headerTintColor: themeColors.white,
    headerStyle: {
      height: 55,

      backgroundColor: '#000',
    },

    headerBackTitle: null,
    headerBackAllowFontScaling: true,
    headerLeftContainerStyle: {
      marginLeft: 0,
    },

    headerTitleStyle: {
      paddingRight: metrics.baseMargin_2x,
      marginLeft: Platform.OS !== 'android' ? 0 : -10,

      fontSize: 17,
      fontFamily: 'OpenSans-Regular',
    },
    headerPressColorAndroid: '#F5F5F5',
    headerRightContainerStyle: { marginRight: metrics.baseContainer },
  },
};

const DefaultNavigator = createStackNavigator(
  {
    Home,
    Sign,
    Profile,
    PasswordReset,
    Product,
    Cart,
    Checkout,
    Category,
    Feedback,
  },
  Config
);

export default DefaultNavigator;
