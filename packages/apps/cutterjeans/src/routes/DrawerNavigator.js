import React from 'react';
import { createDrawerNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Feather';

import { colors } from 'theme';

import DrawerMenu from './DrawerMenu';
import DefaultNavigator from './DefaultNavigator';

const Config = {
  initialRouteName: 'Main',
  contentComponent: DrawerMenu,
  drawerOpenRoute: 'DrawerOpen',
  drawerCloseRoute: 'DrawerClose',
  drawerToggleRoute: 'DrawerToggle',
  defaultNavigationOptions: {
    drawerIcon: () => (
      <Icon
        name="chevron-right"
        size={14}
        color={colors.darker}
        style={{ position: 'absolute', right: -220, top: -8 }}
      />
    ),
  },
};

const DrawerNavigator = createDrawerNavigator(
  {
    Main: { screen: DefaultNavigator },
  },
  Config,
);

export default DrawerNavigator;
