import { createAppContainer } from 'react-navigation';

import DrawerNavigator from './DrawerNavigator';
import DefaultNavigator from './DefaultNavigator';

const Routes = createAppContainer(DrawerNavigator, DefaultNavigator);

export default Routes;
