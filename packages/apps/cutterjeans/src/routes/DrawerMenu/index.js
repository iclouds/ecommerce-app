import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { Alert } from 'react-native';

import {
  Container,
  Content,
  Header,
  Hello,
  Profile,
  ProfileText,
  SignOut,
  LeaveTop,
  FilterItem,
  Description,
  Arrow,
} from './styles';

export default function DrawerMenu({ navigation }) {
  const dispatch = useDispatch();
  const categories = useSelector(state => state.categories);
  const user = useSelector(state => state.user);

  useEffect(() => {
    dispatch({ type: 'GET_CATEGORIES_REQUEST' });
  }, []);

  function renderItems() {
    return (
      <>
        {categories.data
          && categories.data.length > 0
          && categories.data.map((category) => {
            if (category.name === 'Uncategorized') return false;
            return (
              <FilterItem
                key={category.id}
                onPress={() => navigation.navigate('Category', { category: category.name })}
              >
                <Description>{category.name}</Description>
                <Arrow />
              </FilterItem>
            );
          })}
      </>
    );
  }

  function handleSignOut() {
    const alertConfig = [
      { text: 'Cancelar' },
      { text: 'Sair', onPress: () => dispatch({ type: 'SIGN_OUT' }) },
    ];

    Alert.alert('Sair', 'Deseja mesmo sair ?', alertConfig);
  }

  function handleCurrentUserName() {
    if (!!user.data.firstname && user.data.firstname !== '') {
      return user.data.firstname;
    }

    return 'Visitante';
  }

  return (
    <Container>
      <Content forceInset={{ top: 'always', horizontal: 'never' }}>
        <Header>
          <Hello>Olá, {handleCurrentUserName()}</Hello>

          <Profile
            onPress={() => (user.isLoggedIn ? navigation.navigate('Profile') : navigation.navigate('Sign'))
            }
          >
            <ProfileText>{user.isLoggedIn ? 'Minha conta' : 'Entrar / Criar conta'}</ProfileText>
          </Profile>
        </Header>

        {renderItems()}

        {user.isLoggedIn && (
          <SignOut onPress={handleSignOut}>
            <LeaveTop>Sair</LeaveTop>
          </SignOut>
        )}
      </Content>
    </Container>
  );
}

DrawerMenu.propTypes = {
  navigation: PropTypes.shape().isRequired,
};
