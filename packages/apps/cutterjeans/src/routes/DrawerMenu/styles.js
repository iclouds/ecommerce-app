import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/Feather';
import { DrawerItems } from 'react-navigation';

import { colors, metrics, fonts } from 'theme';

export const Container = styled.ScrollView.attrs({
  contentContainerStyle: {
    paddingBottom: 40,
  },
})`
  margin-top: ${metrics.baseMargin_3x}px;
`;

export const Content = styled.SafeAreaView``;

export const Header = styled.View`
  margin: 20px 0 30px 14px;
`;

export const Hello = styled(fonts.size_3x)`
  font-family: 'OpenSans-SemiBold';
  color: ${(props) => props.theme.secondary};
`;

export const Profile = styled.TouchableOpacity.attrs({
  hitSlop: {
    top: 40,
    right: 40,
    bottom: 40,
    left: 40,
  },
})``;

export const ProfileText = styled(fonts.size_1x)``;

export const Items = styled(DrawerItems).attrs({
  itemStyle: {
    marginLeft: 0,
    borderBottomWidth: 1,
    borderBottomColor: '#EEE',

    backgroundColor: '#FFF',
  },
  labelStyle: {
    marginLeft: -35,

    lineHeight: 22,
    color: colors.darker,
    fontSize: 15,
    fontWeight: 'normal',
    fontFamily: 'OpenSans-Regular',
  },
})``;

export const FilterItem = styled.TouchableOpacity.attrs({
  activeOpacity: 0.6,
})`
  border-bottom-width: 1px;
  border-bottom-color: #eee;
  padding-vertical: ${metrics.baseMargin_2x}px;

  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  padding-horizontal: ${metrics.baseContainer}px;
`;

export const Description = styled(fonts.size_3x)``;

export const Arrow = styled(Icon).attrs({
  size: 14,
  color: colors.dark,
  name: 'chevron-right',
})``;

export const SignOut = styled.TouchableOpacity.attrs({
  hitSlop: {
    right: 40,
    bottom: 40,
    left: 40,
  },
})`
  margin: auto 0 20px 12px;
`;

export const LeaveTop = styled(fonts.size_3x)`
  color: ${(props) => props.theme.secondary};
`;
