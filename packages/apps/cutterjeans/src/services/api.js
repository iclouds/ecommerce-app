import axios from 'axios';

// export const baseApi = 'https://api.cutterjeans.iclouds.com.br:8000';
export const baseApi = 'https://cutterjeans.herokuapp.com';

export default axios.create({
  baseURL: baseApi,
});
