import React, { useState, useEffect } from 'react'

import { Container } from './styles'

function Stepper({ action }) {
  const [currentPosition, setCurrentPosition] = useState(0)

  const labels = ['Perfil', 'Endereço', 'Pagamento', 'Confirmar']

  function setStepperPosition(action) {
    if (action.type === 'next') return setCurrentPosition(currentPosition + 1)

    return setCurrentPosition(currentPosition - 1)
  }

  useEffect(() => {
    action && setStepperPosition(action)
  }, [action])

  return (
    <Container
      labels={labels}
      stepCount={labels.length}
      currentPosition={currentPosition}
    />
  )
}

Stepper.defaultProps = {
  action: null,
}

export default Stepper
