import styled from 'styled-components/native'
import StepIndicator from 'react-native-step-indicator'

import { colors } from 'theme'

export const Container = styled(StepIndicator).attrs({
  customStyles: {
    labelSize: 11,
    labelColor: colors.greyest,
    labelFontFamily: 'OpenSans-Regular',

    stepIndicatorLabelFontSize: 11,
    stepIndicatorCurrentColor: colors.white,
    stepIndicatorFinishedColor: colors.orange,
    stepIndicatorUnFinishedColor: colors.white,
    stepIndicatorLabelFinishedColor: colors.white,
    stepIndicatorLabelUnFinishedColor: colors.greyest,
    stepIndicatorLabelCurrentColor: colors.transparent,

    stepIndicatorSize: 10,

    separatorStrokeWidth: 1,
    separatorFinishedColor: colors.orange,
    separatorUnFinishedColor: colors.greyest,

    currentStepStrokeWidth: 6,
    currentStepIndicatorSize: 18,
    currentStepLabelColor: colors.dark,
    currentStepIndicatorLabelFontSize: 11,
    currentStepIndicatorLabelFontFamily: 'OpenSans-Regular',

    stepStrokeWidth: 6,
    stepStrokeCurrentColor: colors.orange,
    stepStrokeFinishedColor: colors.orange,
    stepStrokeUnFinishedColor: colors.greyest,
  },
})``
