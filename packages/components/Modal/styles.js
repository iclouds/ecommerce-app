import styled from 'styled-components/native'
import { Platform } from 'react-native'

import { metrics } from 'theme'

export const Container = styled.Modal.attrs({
  transparent: true,
})``

export const Background = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})`
  flex: 1;
  align-items: center;
  justify-content: flex-end;

  background: rgba(0, 0, 0, 0.6);
`

export const Content = styled.KeyboardAvoidingView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  width: 100%;
  height: ${props => metrics.screenHeight * props.modalHeight}px;
  border-radius: 12px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;

  align-items: center;
  justify-content: center;

  background: #fff;
`
