import React, { useEffect, useState } from 'react'
import { Keyboard, Platform, TouchableWithoutFeedback } from 'react-native'

import { Container, Background, Content } from './styles'

export default function Modal({ children, style, close }) {
  const [modalHeight, setModalHeight] = useState(0.6)

  useEffect(() => {
    if (Platform.OS === 'ios') {
      Keyboard.addListener('keyboardWillHide', () => setModalHeight(0.6))
      Keyboard.addListener('keyboardWillShow', () => setModalHeight(1))
    }
    if (Platform.OS === 'android') {
      Keyboard.addListener('keyboardDidHide', () => setModalHeight(0.6))
      Keyboard.addListener('keyboardDidShow', () => setModalHeight(0.8))
    }
  }, [])

  return (
    <Container>
      <Background onPress={close}>
        <TouchableWithoutFeedback style={{ flex: 1 }}>
          <Content style={style} modalHeight={modalHeight}>
            {children}
          </Content>
        </TouchableWithoutFeedback>
      </Background>
    </Container>
  )
}
