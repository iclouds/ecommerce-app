import React from 'react'
import PlaceholderLib from 'rn-placeholder'

import { Container, PlaceholderImage } from './styles'

export default function Placeholder() {
  return (
    <Container>
      <PlaceholderLib animation="fade">
        <PlaceholderImage />
      </PlaceholderLib>
    </Container>
  )
}
