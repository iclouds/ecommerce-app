import styled from 'styled-components/native';

import { metrics, colors } from "theme"

export const Container = styled.View`
  height: 437px;
  width: 325px;

  margin: ${metrics.baseContainer}px;
`;

export const PlaceholderImage = styled.View`
  position: relative;

  margin-bottom: ${metrics.baseMargin_1x}px;

  height: 100%;
  width: 100%;

  border-radius: ${metrics.baseRadius}px;
  background-color: ${colors.greyest};
`;
