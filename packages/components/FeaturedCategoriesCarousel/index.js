import React, { useState } from 'react'
import { withNavigation } from 'react-navigation'

import Placeholder from './components/Placeholder'

import {
  Item,
  Legend,
  Image,
  Carousel,
  Wrap,
  Action,
  Text,
  Icon,
} from './styles'

const renderLegend = (item, navigation) => {
  return (
    item && (
      <Wrap
        onPress={() =>
          navigation.navigate('Category', { category: item.name })
        }
      >
        <Legend>{item.name}</Legend>

        <Action>
          <Text>Confira</Text>

          <Icon />
        </Action>
      </Wrap>
    )
  )
}

function FeaturedCategoriesCarousel({ items, navigation, isLoaded = false }) {
  function handleOnPressItem(item){
    renderLegend(item, navigation);

    navigation.navigate('Category', { category: item.name })
  }

  function renderItem({ item, index }) {

    return (
      <Item onPress={() => handleOnPressItem(item, index)}>
        <Image source={{ uri: item.src ? item.src : '' }}>
          {renderLegend(item, navigation)}
        </Image>
      </Item>
    )
  }

  function renderCarousel() {
    return (
      <>
        <Carousel
          data={items}
          renderItem={renderItem}
          removeClippedSubviews={false}
        />
      </>
    )
  }

  function renderPlaceHolder() {
    return <Placeholder />
  }

  return isLoaded ? renderCarousel() : renderPlaceHolder()
}

export default withNavigation(FeaturedCategoriesCarousel)
