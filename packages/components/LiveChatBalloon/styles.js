import styled from "styled-components/native";
import { Animated } from "react-native";

import { fonts, colors } from "theme";

export const Container = styled(Animated.View).attrs({
  activeOpacity: 0.9
})`
  position: absolute;
  right: 10px;
  bottom: 30px;
  z-index: 10;

  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const TextBalloon = styled.View`
  position: relative;

  padding: 8px 12px;
  border-radius: 4px;
  margin-right: 8px;

  background: ${props => props.theme.secondary};
`;

export const Triangle = styled.View`
  position: absolute;
  top: 10px;
  right: -6px;
  border-left-width: 7px;
  border-left-color: ${props => props.theme.secondary};

  width: 0;
  height: 0;

  border-top-width: 7px;
  border-top-color: ${colors.transparent};
  border-bottom-width: 7px;
  border-bottom-color: ${colors.transparent};
`;

export const Button = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9
})`
  width: 60px;
  height: 60px;
  border-radius: 60px;
`;

export const Notification = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  z-index: 20;

  width: 16px;
  height: 16px;
  border-radius: 8px;

  align-items: center;
  justify-content: center;

  background: #c63c3c;
`;

export const Text = styled(fonts.size_0x)`
  color: ${colors.white};
`;

export const Avatar = styled.Image`
  width: 100%;
  height: 100%;
  border-radius: 30px;

  resize-mode: cover;
`;
