import React, { useState, useEffect } from "react";
import { Animated, Easing } from "react-native";
import PropTypes from "prop-types";

import {
  Text,
  Avatar,
  Button,
  Triangle,
  Container,
  TextBalloon,
  Notification
} from "./styles";

const animationConfig = {
  duration: 300,
  easing: Easing.bezier(0.215, 0.61, 0.355, 1),
  useNativeDriver: true
};

export default function LiveChat({
  avatar,
  onPress,
  isOnHome,
  isShowing,
  notifications
}) {
  const [animations, _] = useState({
    opacity: new Animated.Value(0),
    right: new Animated.Value(100)
  });

  function hideAndShowAnimation(isShowing) {
    return Animated.parallel([
      Animated.timing(animations.opacity, {
        ...animationConfig,
        toValue: isShowing ? 1 : 0
      }).start(),

      Animated.timing(animations.right, {
        ...animationConfig,
        toValue: isShowing ? -12 : 100
      }).start()
    ]);
  }

  useEffect(() => {
    function handleShowAndHide() {
      if (isShowing) {
        return hideAndShowAnimation(isShowing);
      }

      return hideAndShowAnimation(isShowing);
    }

    handleShowAndHide();
  }, [isShowing]);

  return (
    <Container
      style={{
        opacity: animations.opacity,
        transform: [{ translateX: animations.right }]
      }}
    >
      {isOnHome && (
        <TextBalloon>
          <Triangle />

          <Text>Toque na foto para falar com Sabrina</Text>
        </TextBalloon>
      )}

      <Button onPress={onPress}>
        <Notification>
          <Text>{notifications}</Text>
        </Notification>

        <Avatar source={{ uri: "https://cutterjeans.herokuapp.com/files" }} />
      </Button>
    </Container>
  );
}

LiveChat.propTypes = {
  notifications: PropTypes.number.isRequired
};
