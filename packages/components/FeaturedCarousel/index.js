import React, { useState } from 'react'
import { Modal, Platform } from 'react-native'
import { withNavigation } from 'react-navigation'
import ImageViewer from 'react-native-image-zoom-viewer'

import { FeaturedCarouselPlaceholder } from 'components'

import {
  Item,
  Legend,
  Image,
  Carousel,
  Wrap,
  Action,
  Text,
  Icon,
  Close,
  CloseIcon,
} from './styles'

const handleLegend = (item, navigation) => {
  return (
    item &&
    item.images && (
      <Wrap
        onPress={() =>
          navigation.navigate('Product', {
            productId: item.id,
            productName: item.name,
          })
        }
      >
        <Legend>{item.name}</Legend>

        <Action>
          <Text>Confira</Text>

          <Icon />
        </Action>
      </Wrap>
    )
  )
}

function FeaturedCarousel({ items, navigation, isLoaded = false, showDetailsOnPress = false, showModalOnPress = false }) {
  const [modalVisibility, setModalVisibility] = useState(false)
  const [productIndex, setProductIndex] = useState(0)

  function handleOnPressItem(item, index){
    handleLegend(item, navigation);
    if(showDetailsOnPress){
      navigation.navigate('Product', {
        productId: item.id,
        productName: item.name,
      })
    }

    if(showModalOnPress)  {
      setProductIndex(index);
      setModalVisibility(true);
    }
  }

  function renderItem({ item, index }) {

    return (
      <Item
        onPress={() => handleOnPressItem(item, index)}
      >
        <Image source={{ uri: item.images ? item.images[0].src : item.src }}>
          {handleLegend(item, navigation)}
        </Image>
      </Item>
    )
  }

  function getImagesFromProduct(productImages) {
    return productImages.map(item => {
      return {
        url: item.src,
        width: 100,
      }
    })
  }

  function renderCarousel() {
    return (
      <>
        <Carousel
          data={items}
          renderItem={renderItem}
          removeClippedSubviews={false}
        />
        <Modal
          visible={modalVisibility}
          transparent={true}
          animationType="fade"
          hardwareAccelerated
        >
          <ImageViewer
            style={{ paddingTop: Platform.OS === 'ios' ? 32 : 0 }}
            imageUrls={getImagesFromProduct(
              items[0] && items[0].images ? items[productIndex].images : items
            )}
            backgroundColor="#141414"
            renderHeader={() => (
              <Close onPress={() => setModalVisibility(false)}>
                <CloseIcon />
              </Close>
            )}
          />
        </Modal>
      </>
    )
  }

  function renderCarouselPlaceHolder() {
    return <FeaturedCarouselPlaceholder />
  }

  return isLoaded ? renderCarousel() : renderCarouselPlaceHolder()
}

export default withNavigation(FeaturedCarousel)
