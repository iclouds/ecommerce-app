import styled from 'styled-components/native'
import { Dimensions } from 'react-native'
import CarouselLib from 'react-native-snap-carousel'
import IconLib from 'react-native-vector-icons/MaterialCommunityIcons'
import { Platform } from 'react-native'

import { fonts, metrics, colors } from 'theme'

const { width } = Dimensions.get('window')

export const Item = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})`
  position: relative;

  height: 437px;
  width: 100%;
`

export const Image = styled.ImageBackground`
  width: 100%;
  height: 100%;
  border-radius: 4px;

  justify-content: flex-end;

  overflow: hidden;
  resize-mode: cover;
`

export const Wrap = styled.TouchableOpacity.attrs({
  activeOpacity: 0.8,
})`
  width: 100%;
  padding: ${metrics.baseContainer}px;

  background-color: rgba(0, 0, 0, 0.74);
`

export const Legend = styled(fonts.size_5x)`
  line-height: 24px;
  color: ${colors.white};
  font-family: 'OpenSans-SemiBold';
`

export const Action = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
`

export const Text = styled(fonts.size_2x)`
  margin-right: 4px;

  color: ${colors.brown};
  letter-spacing: -0.33px;
`

export const Icon = styled(IconLib).attrs({
  size: 20,
  name: 'arrow-right',
  color: colors.brown,
})``

export const Carousel = styled(CarouselLib).attrs({
  left: -15,
  itemWidth: width * 0.888,
  sliderWidth: width + 15,
  marginTop: metrics.baseContainer,
})``

export const Container = styled.ScrollView`
  padding: ${metrics.baseContainer}px;
  margin-bottom: ${Platform.OS !== 'android' ? metrics.baseMargin_2x : 0}px;
  background: red;
`

export const Products = styled.View`
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: space-between;

  padding-bottom: ${metrics.baseContainer}px;

  background: #141414;
`

export const Close = styled.TouchableOpacity.attrs({
  activeOpacity: 1,
})`
  background: #141414;
`

export const CloseIcon = styled(IconLib).attrs({
  name: 'close',
  color: '#FFF',
  size: 30,
})`
  margin-left: auto;
  margin-top: 10px;
  margin-right: 10px;
`
