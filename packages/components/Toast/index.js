import React from 'react'
import ToastNotification from 'react-native-toast-notification'

export default function Toast({ text, backgroundColor, textColor }) {
  return (
    <ToastNotification
      text={text}
      duration={5000}
      textStyle={{ color: textColor }}
      style={{ backgroundColor: backgroundColor, marginBottom: -40 }}
    />
  )
}
