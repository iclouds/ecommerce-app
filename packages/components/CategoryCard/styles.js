import styled from 'styled-components';

export const Container = styled.View`
  width: 153px;
  height: 77px;
  border-radius: 4px;
  background-color: #a2723c;
`;
