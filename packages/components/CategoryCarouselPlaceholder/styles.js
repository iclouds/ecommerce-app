import styled from 'styled-components';

import { colors, metrics } from 'theme';

export const Container = styled.View`
  margin: ${metrics.baseContainer}px;

  width: 100%;
`;

export const Wrap = styled.ScrollView.attrs({
  horizontal: true,
  showsHorizontalScrollIndicator: false,
})``;

export const Image = styled.View`
  margin-right: ${metrics.baseMargin_1x}px;

  width: 153px;
  height: 77px;

  border-radius: 4px;
  background-color: ${colors.light}
`;
