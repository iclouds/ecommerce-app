import React from 'react';

import Placeholder from 'rn-placeholder'


import { Wrap, Image, Container } from './styles';

export default function CategoryCarouselPlaceholder(){
  return (
    <Container>
      <Placeholder animation="fade">
        <Wrap>
            <Image />
            <Image />
            <Image />
        </Wrap>
      </Placeholder>
    </Container>
  )
}
