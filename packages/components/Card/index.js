import React from "react";
import PropTypes from "prop-types";

import { ConvertFloatToMoney } from "functions";

import {
  Row,
  Title,
  Content,
  Container,
  CustomImage,
  Description,
  ImageContainer
} from "./styles";

function Card({ productProps, listProps, isTotal }) {
  function renderProduct() {
    const { name, size, color, amount, price, images } = productProps;

    const formatedPrice = ConvertFloatToMoney(price);

    return (
      <>
        <ImageContainer>
          <CustomImage source={{ uri: images[0].src }} />
        </ImageContainer>

        <Content>
          <Row>
            <Title />

            <Description>{`${name} - ${size}`}</Description>
          </Row>

          <Row>
            <Title />

            <Description>{amount}</Description>
          </Row>

          <Row lastChild>
            <Title />

            <Description>{formatedPrice}</Description>
          </Row>
        </Content>
      </>
    );
  }

  function renderList() {
    function isLastItem(array, index) {
      const lastArrayPosition = array.length - 1;

      return lastArrayPosition === index;
    }

    const lastObjectItem = Object.keys(listProps);

    return (
      <Content list isTotal={isTotal}>
        {listProps.map((item, index) => (
          <Row
            key={index}
            isTotal={isTotal}
            lastChild={isLastItem(lastObjectItem, index)}
          >
            <Title
              isTotal={isTotal}
              lastChild={isLastItem(lastObjectItem, index)}
            >
              {item.title}
            </Title>

            <Description
              isTotal={isTotal}
              lastChild={isLastItem(lastObjectItem, index)}
            >
              {item.description}
            </Description>
          </Row>
        ))}
      </Content>
    );
  }

  return (
    <Container>
      {productProps && renderProduct()}

      {listProps && renderList()}
    </Container>
  );
}

Card.defaultProps = {
  listProps: [],
  productProps: null,
  isTotal: false
};

Card.propTypes = {
  listProps: PropTypes.arrayOf(PropTypes.shape()),
  productProps: PropTypes.shape(),
  isTotal: PropTypes.bool
};

export default Card;
