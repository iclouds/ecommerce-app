import styled from 'styled-components/native'

import { metrics, colors, fonts } from 'theme'

export const Container = styled.View.attrs({
  style: {},
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.25,
  shadowRadius: 3.84,

  elevation: 5,
})`
  flex-direction: row;
  justify-content: space-between;

  width: ${metrics.screenWidth - metrics.baseContainer * 2}px;
  border-radius: ${metrics.baseRadius}px;
  margin: ${metrics.baseContainer}px;
  margin-bottom: ${metrics.baseMargin_1x}px;

  background-color: white;
`

export const ImageContainer = styled.View`
  width: 25%;
  height: 93.7px;
  overflow: hidden;

  border-top-left-radius: ${metrics.baseRadius}px;
  border-bottom-left-radius: ${metrics.baseRadius}px;
`

export const CustomImage = styled.Image`
  width: 100%;
  height: 100%;
`

export const Content = styled.View`
  width: ${props => (props.list ? '100' : '75')}%;
  border-radius: ${metrics.baseRadius}px;
  padding: ${props => (props.isTotal ? 0 : metrics.baseMargin_1x)}px;
  padding-top: ${metrics.baseMargin_1x}px;
`

export const Row = styled.View`
  width: 100%;
  border-bottom-left-radius: ${metrics.baseRadius}px;
  border-bottom-right-radius: ${metrics.baseRadius}px;
  padding-vertical: ${props =>
    props.isTotal && props.lastChild ? metrics.baseMargin_1x : 0}px;
  padding-horizontal: ${props =>
    !props.isTotal ? 0 : metrics.baseMargin_1x}px;
  margin-bottom: ${props => (props.lastChild ? 0 : metrics.baseMargin_1x)}px;

  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  background: ${props =>
    props.isTotal && props.lastChild ? props.theme.secondaryLight : 'white'};
`

export const Title = styled(fonts.size_2x)`
  font-weight: bold;
  color: ${props =>
    props.isTotal && props.lastChild ? props.theme.secondary : colors.grey};
`

export const Description = styled(fonts.size_1x).attrs({
  numberOfLines: 1,
})`
  max-width: 85%;

  text-align: right;
  font-weight: ${props => (props.isTotal && props.lastChild ? 500 : 400)};
  color: ${props =>
    props.isTotal && props.lastChild ? props.theme.secondary : colors.grey};
`
