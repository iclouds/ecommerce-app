import styled from "styled-components/native";
import { Platform, Animated } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import { fonts, colors, metrics } from "theme";

export const Container = styled(Animated.View).attrs(props => ({
  elevation: props.isVisible ? 10 : -10
}))`
  position: absolute;
  top: 0;
  left: 0;

  width: 100%;
  height: 100%;

  background: rgba(50, 50, 50, 0.9);
`;

export const Header = styled.View`
  width: 100%;
  height: ${Platform.OS === "android" ? 60 : 100}px;
  padding: ${Platform.OS === "android" ? 0 : 30}px 16px 0;

  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  background: ${props => props.theme.secondary};
`;

export const Wrap = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const Avatar = styled.Image`
  width: 42px;
  height: 42px;
  margin-right: 8px;
  border-radius: 21px;
`;

export const Info = styled.View``;

export const Name = styled(fonts.size_2x)`
  margin-top: 3px;

  color: ${colors.white};
  font-family: "Heebo-Bold";
`;

export const Role = styled(fonts.size_0x)`
  margin-top: -3px;

  color: ${colors.white};
  font-family: "Heebo-Medium";
`;

export const Close = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9
})``;

export const CloseIcon = styled(Icon).attrs({
  size: 26,
  name: "close",
  color: colors.white
})``;

export const Content = styled.KeyboardAvoidingView.attrs({
  behavior: Platform.OS !== "android" && "padding"
})`
  flex: 1;
`;

export const WriteMessage = styled.View`
  height: 50px;
  padding: 0 12px;
  border-radius: 4px;
  margin: 0 ${metrics.baseContainer}px 22px;

  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  background: ${colors.white};
`;

export const Input = styled.TextInput.attrs({
  placeholder: "Digite uma mensagem...",
  placeholderTextColor: colors.grey
})`
  width: 90%;
  height: 100%;
`;

export const Send = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
  hitSlop: { top: 30, left: 30, bottom: 30, right: 30 }
})``;

export const SendIcon = styled(Icon).attrs(props => ({
  size: 24,
  name: "send",
  color: props.isFocused ? props.theme.secondary : colors.greyest
}))``;

export const Messages = styled.FlatList.attrs({
  inverted: true,
  showsVerticalScrollIndicator: false
})`
  margin: 0 ${metrics.baseContainer}px 6px;
`;
