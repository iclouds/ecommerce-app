import React, { useState, useEffect } from "react";
import { Animated, Easing } from "react-native";
import PropTypes from "prop-types";
import openSocket from "socket.io-client";
import axios from "axios";

import Message from "./Message";
import Modal from "./StartChatModal";

import {
  Container,
  Header,
  Wrap,
  Avatar,
  Info,
  Name,
  Role,
  Close,
  CloseIcon,
  Content,
  WriteMessage,
  Input,
  Send,
  SendIcon,
  Messages
} from "./styles";

const animationConfig = {
  duration: 300,
  useNativeDriver: true,
  easing: Easing.bezier(0.215, 0.61, 0.355, 1)
};

export default function Messenger({
  user,
  close,
  baseApi,
  messages,
  adminInfo,
  isVisible,
  navigator,
  dispatchNewUser,
  dispatchNewMessage,
  dispatchLoadMessages,
  dispatchUnseenMessage,
  dispatchRemoveUnseenMessages
}) {
  const [message, setMessage] = useState("");
  const [incomingMessage, setIncomingMessage] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(
    !!user.firstname && user.firstname.length === 0
  );
  const [isTheChatOpen, setIsTheChatOpen] = useState(false);

  const [isInputFocused, setIsInputFocused] = useState(false);
  const [animations, _] = useState({
    opacity: new Animated.Value(0),
    top: new Animated.Value(2500)
  });

  const [socketID, setSocketID] = useState("");

  const socket = openSocket.connect(baseApi, {
    transports: ['websocket'],
    secure: false
  });

  function isUserRegister() {
    return user.firstname && user.firstname.length > 0;
  }

  function handleStartChatModal() {
    if (isUserRegister()) {
      return setIsModalOpen(false);
    }

    return setIsModalOpen(true);
  }

  function handleStartChat() {
    if (isUserRegister() && socketID.length > 0) {
      socket.emit("startChat", {
        id: socketID,
        name: user.firstname,
        ...user
      });
    }
  }

  useEffect(() => {
    handleStartChatModal();

    handleStartChat();
  }, [user, socketID]);

  useEffect(() => {
    function handleOpenAnimation() {
      Animated.parallel([
        Animated.timing(animations.opacity, {
          ...animationConfig,
          toValue: 1
        }).start(),

        Animated.timing(animations.top, {
          ...animationConfig,
          toValue: 0
        }).start()
      ]);
    }

    handleStartChatModal();

    if (isVisible) {
      handleOpenAnimation();
      dispatchRemoveUnseenMessages({ type: "REMOVE_UNSEEN_MESSAGES" });
      setIsTheChatOpen(true);
    } else {
      setIsTheChatOpen(false);
    }
  }, [isVisible]);

  useEffect(() => {
    socket.on("connect", function(data) {
      setSocketID(socket.id);
    });

    socket.on("message", function(newMessage) {
      setIncomingMessage(newMessage);
    });

    async function getPreviousMessages() {
      try {
        const { data } = await axios.get(`${baseApi}/chat`, {
          params: {
            email: user.email
          }
        });

        const previousMessages = data[0].messages;

        function unreadChatHistory() {
          return previousMessages.length - messages.length;
        }

        dispatchLoadMessages({
          previousMessages,
          unreadMessages: unreadChatHistory()
        });
      } catch (err) {
        //alert("(Messenger) algo deu errado!");
      }
    }

    getPreviousMessages();
  }, [user]);

  useEffect(() => {
    function handleNewMessage() {
      if (incomingMessage) {
        dispatchNewMessage(incomingMessage);

        setMessage("");

        if (!isTheChatOpen) {
          dispatchUnseenMessage({ type: "ADD_UNSEEN_MESSAGE" });
        }
      }
    }

    handleNewMessage();
  }, [incomingMessage]);

  function handleClose() {
    Animated.parallel([
      Animated.timing(animations.opacity, {
        ...animationConfig,
        toValue: 0
      }).start(),

      Animated.timing(animations.top, {
        ...animationConfig,
        toValue: 1000
      }).start()
    ]);

    setTimeout(() => {
      close();
    }, 300);
  }

  function handleSubmit() {
    const newMessage = {
      id: socketID,
      text: message,
      email: user.email,
      name: user.firstname
    };

    setMessage("");

    if (isUserRegister()) {
      socket.emit("customerMessage", newMessage);
    }
  }

  const { name, role, avatar } = adminInfo;

  return (
    <Container
      isVisible={isVisible}
      style={{
        opacity: animations.opacity,
        transform: [{ translateY: animations.top }],
        zIndex: isVisible ? 10 : -10
      }}
      elevation={isVisible ? 10 : -10}
    >
      <Header>
        <Wrap>
          <Avatar source={{ uri: avatar }} />

          <Info>
            <Name>{name}</Name>

            <Role>{role}</Role>
          </Info>
        </Wrap>

        <Close onPress={handleClose}>
          <CloseIcon />
        </Close>
      </Header>

      <Content>
        <Messages
          data={messages}
          renderItem={({ item }) => (
            <Message
              isUser={!item.admin}
              text={item.text}
              time={item.time}
              avatar={avatar}
            />
          )}
          keyExtractor={(item, index) => String(`${index}-${item.text}`)}
        />

        <WriteMessage>
          <Input
            value={message}
            autoCorrect={true}
            onFocus={() => {}}
            returnKeyType="send"
            returnKeyLabel="Enviar"
            onChangeText={setMessage}
            onSubmitEditing={() => handleSubmit()}
            onFocus={() => setIsInputFocused(true)}
            onBlur={() => setIsInputFocused(false)}
          />

          <Send onPress={handleSubmit} disabled={message.length <= 0}>
            <SendIcon isFocused={isInputFocused} />
          </Send>
        </WriteMessage>
      </Content>

      {isModalOpen && isVisible && (
        <Modal
          user={user}
          navigation={navigator}
          dispatchNewUser={dispatchNewUser}
          closeModal={() => setIsModalOpen(false)}
        />
      )}
    </Container>
  );
}

Messenger.propTypes = {
  close: PropTypes.func,
  isVisible: PropTypes.bool.isRequired
};
