import styled, { css } from 'styled-components/native'

import { fonts, colors, metrics } from 'theme'

export const Container = styled.View`
  max-width: ${metrics.screenWidth / 2};
  margin-top: 20px;
  margin-right: 7px;
  margin-left: ${props => (props.isUser ? 'auto' : '0')};

  flex-direction: ${props => (props.isUser ? 'column' : 'row')};
  align-items: flex-start;
`

export const AvatarWrap = styled.View`
  align-items: center;
  justify-content: center;
`

export const Avatar = styled.Image`
  width: 40px;
  height: 40px;
  border-radius: 20px;
`

export const Time = styled(fonts.size_0x)`
  color: ${colors.greyest};
`

export const TextBalloon = styled.View`
  position: relative;

  padding: 8px 12px;
  border-radius: 4px;
  margin-left: 8px;

  background: ${props => (props.isUser ? props.theme.secondary : colors.light)};
`

export const Triangle = styled.View`
  position: absolute;
  top: 12px;
  ${props =>
    props.isUser
      ? css`
          right: -6px;
          border-left-width: 7px;
          border-left-color: ${props.theme.secondary};
        `
      : css`
          left: -6px;
          border-right-width: 7px;
          border-right-color: ${colors.light};
        `}

  width: 0;
  height: 0;

  border-top-width: 7px;
  border-top-color: ${colors.transparent};
  border-bottom-width: 7px;
  border-bottom-color: ${colors.transparent};
`

export const Text = styled(fonts.size_2x)`
  line-height: 18;
  color: ${props => (props.isUser ? colors.light : props.theme.secondary)};
`
