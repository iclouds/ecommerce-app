import React from "react";
import PropTypes from "prop-types";
import { format, parseISO } from "date-fns";

import {
  Container,
  AvatarWrap,
  Avatar,
  Time,
  TextBalloon,
  Triangle,
  Text
} from "./styles";

const balloonShadow = {
  shadowColor: "#333",
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowOpacity: 0.25,
  shadowRadius: 3.84,

  elevation: 5
};

export default function Message({ isUser, text, time, avatar }) {
  function formatHourAndMinutes() {
    const parsedTime = parseISO(time);

    return format(parsedTime, "HH:mm");
  }

  function renderEmployeeMessage() {
    return (
      <>
        <AvatarWrap>
          <Avatar source={{ uri: avatar }} />

          <Time>{time ? formatHourAndMinutes() : ""}</Time>
        </AvatarWrap>

        <TextBalloon style={balloonShadow}>
          <Triangle />

          <Text>{text}</Text>
        </TextBalloon>
      </>
    );
  }

  function renderUserMessage() {
    return (
      <>
        <TextBalloon isUser style={balloonShadow}>
          <Triangle isUser />

          <Text isUser>{text}</Text>
        </TextBalloon>

        <Time isUser style={{ marginLeft: "auto" }}>
          {time ? formatHourAndMinutes() : ""}
        </Time>
      </>
    );
  }

  return (
    <Container isUser={isUser}>
      {isUser ? renderUserMessage() : renderEmployeeMessage()}
    </Container>
  );
}

Message.defaultProps = {
  isUser: false
};

Message.propTypes = {
  isUser: PropTypes.bool
};
