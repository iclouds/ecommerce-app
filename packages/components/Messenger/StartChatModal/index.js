import React from 'react'
import * as Yup from 'yup'

import { InputFactory } from 'functions'

import { Container, Form, Title } from './styles'

const schema = Yup.object().shape({
  firstname: Yup.string().required('Insira seu nome!'),
  email: Yup.string()
    .email('Insira um email válido')
    .required('Insira seu email!'),
})

export default function StartChatModal({
  user,
  navigator,
  closeModal,
  dispatchNewUser,
}) {
  const inputs = [
    {
      labelText: 'Primeiro nome',
      type: 'firstname',
      returnKeyType: 'next',
    },
    {
      type: 'email',
      labelText: 'Email',
      autoCorrect: true,
      returnKeyType: 'send',
      autoCompleteType: 'email',
      keyboardType: 'email-address',
      value: user.email,
    },
  ]

  function onSubmit(data) {
    dispatchNewUser(data)
  }

  return (
    <Container close={closeModal}>
      <Title>CutterChat</Title>

      <Form
        schema={schema}
        submit={onSubmit}
        navigator={navigator}
        buttonText="CONVERSAR"
        inputs={InputFactory(inputs)}
      />
    </Container>
  )
}
