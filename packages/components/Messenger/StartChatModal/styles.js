import styled from 'styled-components/native'
import { Platform } from 'react-native'

import Modal from '../../Modal'
import FormComponent from '../../Form'

import { fonts } from 'theme'

export const Container = styled(Modal)`
  padding: 0 20px 80px;
`

export const Title = styled(fonts.size_6x)`
  font-size: 30px;
  font-family: ${Platform.OS === 'android' ? 'yumin' : 'YuMincho-Regular'};
`

export const Form = styled(FormComponent)`
  width: 100%;
`
