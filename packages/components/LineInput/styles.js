import styled, { css } from 'styled-components/native'
import posed from 'react-native-pose'
import { Animated } from 'react-native'
import IconLib from 'react-native-vector-icons/MaterialCommunityIcons'

import { colors, fonts } from 'theme'

export const Container = styled(Animated.View).attrs(props => ({
  style: props.style,
}))`
  position: relative;
`

export const AnimatedLabel = posed.Text({
  isNotFocused: {
    bottom: 7,
    fontSize: 16,
  },
  isFocused: {
    bottom: 34,
    fontSize: 12,
  },
})

export const Label = styled(AnimatedLabel)`
  position: absolute;
  left: 1px;
  bottom: 8px;

  color: #333;
  opacity: 0.5;
  font-family: 'OpenSans-Regular';
`

export const InputField = styled.TextInput.attrs({
  placeholderTextColor: colors.greyest,
})`
  padding: 7px 0;

  text-align: left;
  border-bottom-width: ${props => (props.isFocused ? 3 : 1)}px;
  border-bottom-color: ${props =>
    props.error
      ? colors.danger
      : props.isFocused
      ? props.theme.secondary
      : 'rgba(189, 189, 189, 1)'};

  background: transparent;
  font-family: 'OpenSans-Regular';
  color: #333;
`

export const EyeIcon = styled.TouchableOpacity.attrs({
  activeOpacity: 0.8,
  hitSlop: { top: 20, right: 20, bottom: 20, left: 20 },
})`
  position: absolute;
  right: 0;
  bottom: 4px;
`

export const Icon = styled(IconLib).attrs(props => ({
  size: 24,
  color: colors.greyest,
  name: props.isHide ? 'eye-outline' : 'eye-off-outline',
}))``

export const Error = styled(fonts.size_1x)`
  position: absolute;
  right: 0;
  bottom: -20px;

  color: ${colors.danger};
`
