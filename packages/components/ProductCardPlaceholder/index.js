import React from 'react'
import PropTypes from 'prop-types'
import Placeholder, { Line } from 'rn-placeholder'

import { Container, PlaceholderImage } from './styles'

export default function ProductCardPlaceholder({ fullWidth }) {
  return (
    <Container fullWidth={fullWidth}>
      <Placeholder animation="fade">
        <PlaceholderImage />

        <Line width="80%" />

        <Line width="45%" />
      </Placeholder>
    </Container>
  )
}

ProductCardPlaceholder.defaultProps = {
  fullWidth: false,
}

ProductCardPlaceholder.propTypes = {
  fullWidth: PropTypes.bool,
}
