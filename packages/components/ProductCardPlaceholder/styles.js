import styled from 'styled-components/native';

import { metrics, colors } from "theme"

export const Container = styled.View`
  width: ${ props => props.fullWidth ? 100 : 47 }%;
  margin-bottom: ${metrics.baseMargin_2x}px;
`;

export const PlaceholderImage = styled.View`
  height: 180px;
  border-radius: ${metrics.baseRadius}px;
  margin-bottom: ${metrics.baseMargin_1x}px;

  background-color: ${colors.light};
`;
