import React, { useState, useEffect } from 'react'

import { colors } from 'theme'

import { ActivityIndicator } from 'react-native'
import PropTypes from 'prop-types'
import { View } from 'react-native'

import { RemoveSpecialCharacters } from 'functions'

import {
  Number,
  UpIcon,
  CartIcon,
  DownIcon,
  PlusIcon,
  Container,
  CustomIcon,
  Description,
  PrimaryAction,
  SecondaryIcons,
  SecondaryAction,
  CartIconContainer,
  SecondaryActionButton,
} from './styles'

export default function ActionFooter({
  cart,
  input,
  disabled,
  monoIcon,
  isLoading,
  showToast,
  monoAction,
  primaryText,
  selectItemToCart,
  stockProductsAmount,
  primaryActionOnPress,
  secondaryActionOnPress,
}) {
  const [amount, setAmount] = useState('1')
  const [insideDisabled, setInsideDisabled] = useState(disabled)

  useEffect(() => {
    if (amount >= 1) {
      selectItemToCart({ amount })
    }
  }, [amount])

  function inputMaxValue(limit, amount) {
    const parsedLimit = parseInt(limit, 10)
    const parsedAmount = parseInt(amount, 10)

    if (limit === null) return '1'
    if (amount === '0') return setAmount('1')

    if (parsedAmount > parsedLimit) {
      showToast('warn')
      return setInsideDisabled(true)
    }

    return setInsideDisabled(false)
  }

  function renderSecondaryAction() {
    return (
      <>
        {input && (
          <SecondaryAction disabled={disabled}>
            <Number
              editable={!disabled}
              onChangeText={value => (
                setAmount(RemoveSpecialCharacters(value)),
                inputMaxValue(stockProductsAmount, value)
              )}
              value={amount}
            />

            {/* <SecondaryIcons>
              <UpIcon />
              <DownIcon />
            </SecondaryIcons> */}
          </SecondaryAction>
        )}

        {!input && (
          <SecondaryActionButton
            onPress={() => secondaryActionOnPress()}
          >
            <CustomIcon name="arrow-left" />
          </SecondaryActionButton>
        )}
      </>
    )
  }

  function renderPrimaryAction() {
    return (
      <PrimaryAction
        disabled={disabled}
        onPress={() => primaryActionOnPress()}
      >
        { isLoading ?
          <ActivityIndicator size="large" color={colors.light} />
          : <Description>{primaryText}</Description>
        }

        {/* {cart && (
          <CartIconContainer>
            <CartIcon />
            <PlusIcon />
          </CartIconContainer>
        )} */}

        {!cart && !isLoading && <CustomIcon name="arrow-right" />}
      </PrimaryAction>
    )
  }

  function renderMonoAction() {
    return (
      <PrimaryAction
        disabled={disabled}
        onPress={() => primaryActionOnPress()}
        style={!monoIcon && { justifyContent: 'center' }}
      >
        {monoIcon && <View />}


        { isLoading ?
          <ActivityIndicator size="large" color={colors.light} />
          : <Description>{primaryText}</Description>
        }

        {monoIcon && !isLoading && <CustomIcon name="arrow-right" />}
      </PrimaryAction>
    )
  }

  return (
    <Container>
      {!monoAction && renderSecondaryAction()}
      {!monoAction && renderPrimaryAction()}
      {monoAction && renderMonoAction()}
    </Container>
  )
}

ActionFooter.defaultProps = {
  cart: false,
  input: false,
  monoIcon: false,
  isLoading: false,
  monoAction: false,
  selectItemToCart: () => {},
  secondaryActionOnPress: () => {},
}

ActionFooter.propTypes = {
  cart: PropTypes.bool,
  input: PropTypes.bool,
  monoIcon: PropTypes.bool,
  isLoading: PropTypes.bool,
  monoAction: PropTypes.bool,
  selectItemToCart: PropTypes.func,
  secondaryActionOnPress: PropTypes.func,
  primaryText: PropTypes.string.isRequired,
  primaryActionOnPress: PropTypes.func.isRequired,
}
