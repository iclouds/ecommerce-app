import styled from 'styled-components/native'
import { Platform } from 'react-native'
import { isIphoneX } from 'react-native-iphone-x-helper'
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import FeatherIcon from 'react-native-vector-icons/Feather'

import { colors, fonts, metrics } from 'theme'

export const Container = styled.View`
  position: absolute;
  bottom: 0;

  width: 100%;
  height: ${!isIphoneX() ? 50 : 70}px;

  flex-direction: row;
`

export const SecondaryAction = styled.View`
  flex: 0.4;
  align-items: center;
  flex-direction: row;
  justify-content: center;

  padding-top: 2px;

  background: ${props =>
    props.disabled ? colors.grey : props.theme.secondaryVariant};
`

export const SecondaryActionButton = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})`
  flex: 0.2;
  align-items: center;
  flex-direction: row;
  justify-content: center;

  padding-bottom: ${isIphoneX() ? 16 : 0}px;

  background: ${props =>
    props.disabled ? colors.grey : props.theme.secondaryVariant};
`

export const Number = styled.TextInput.attrs({
  keyboardType: 'numeric',
  maxLength: 3,
})`
  position: absolute;

  width: 100%;
  padding-bottom: ${!isIphoneX() ? 8 : 14}px;

  text-align: center;
  color: ${colors.white};
  font-weight: 600;
  letter-spacing: -0.33px;
`

export const CustomIcon = styled(MaterialIcon).attrs({
  size: 20,
  color: colors.white,
})`
  margin-left: 5px;
  padding-top: 2px;
`

export const PrimaryAction = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})`
  flex: 1;
  align-items: center;
  flex-direction: row;
  justify-content: center;

  padding: 0;
  padding-bottom: ${isIphoneX() ? metrics.baseMargin_2x : 0}px;

  background: ${props =>
    props.disabled ? colors.greyDark : props.theme.secondary};
`

export const Description = styled(fonts.size_2x)`
  font-weight: 600;
  letter-spacing: 0.33px;
  color: ${colors.white};
  font-family: OpenSans-Regular;
`

export const CartIconContainer = styled.View`
  flex-direction: row;
`

export const CartIcon = styled(FeatherIcon).attrs({
  size: 18,
  color: colors.white,
  name: 'shopping-cart',
})`
  padding-bottom: ${!isIphoneX() ? 0 : 12}px;
`

export const PlusIcon = styled(MaterialIcon).attrs({
  size: 14,
  name: 'plus-circle',
  color: colors.white,
})`
  position: absolute;
  top: -6;
  right: -7;
`

export const SecondaryIcons = styled.View`
  position: absolute;
  right: 2px;

  justify-content: center;
  align-items: center;
`

export const DownIcon = styled(CustomIcon).attrs({
  name: 'menu-down',
})`
  margin-top: -7px;
`

export const UpIcon = styled(CustomIcon).attrs({
  name: 'menu-up',
})`
  margin-bottom: -7px;
`
