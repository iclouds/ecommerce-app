import React from "react";
import PropTypes from "prop-types";
import { ActivityIndicator } from "react-native";

import { colors } from "theme";

import { Container, Description, CustomIcon } from "./styles";

export default function Button({
  text,
  hasIcon,
  color,
  isLoading,
  isChecked,
  ...rest
}) {
  return (
    <Container {...rest} isChecked={isChecked}>
      {isLoading ? (
        <ActivityIndicator size="small" color={colors.white} />
      ) : (
        <Description color={color} hasIcon={hasIcon}>
          {text}
        </Description>
      )}

      {hasIcon && <CustomIcon isChecked={isChecked} />}
    </Container>
  );
}

Button.defaultProps = {
  hasIcon: false,
  isChecked: false,
  color: colors.greyest,
  isLoading: false
};

Button.propTypes = {
  color: PropTypes.string,
  hasIcon: PropTypes.bool,
  isChecked: PropTypes.bool,
  isLoading: PropTypes.bool,
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired
};
