import styled, { css } from "styled-components/native";
import Icon from "react-native-vector-icons/Feather";

import { fonts, metrics, colors } from "theme";

export const Container = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9
})`
  width: 160px;
  height: 40px;
  border-radius: ${metrics.baseRadius}px;

  flex-direction: row;
  align-items: center;
  justify-content: space-around;

  background: ${props =>
    props.isChecked ? props.theme.secondary : props.theme.greyest};
`;

export const Description = styled(fonts.size_1x)`
  color: ${colors.white};
  font-family: ${props =>
    props.hasIcon ? "OpenSans-Regular" : "OpenSans-Bold"};
  opacity: ${props => (props.color && 1) || 0.6};

  ${props =>
    props.hasIcon
      ? css`
          font-family: "OpenSans-Regular";
          font-size: 14;
        `
      : css`
          font-family: "OpenSans-Bold";
        `};
`;

export const CustomIcon = styled(Icon).attrs(props => ({
  size: 14,
  name: props.isChecked ? "check" : "x",
  color: colors.white
}))``;
