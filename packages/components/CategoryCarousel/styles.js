import styled from 'styled-components/native';

import { fonts, metrics, colors } from 'theme';


export const Container = styled.ScrollView.attrs({
  showsHorizontalScrollIndicator: false,
})`
  margin-top: 11px;

  padding-left: ${metrics.baseContainer}px;
`

export const Item = styled.View`
  position: relative;

  width: 153px;
  height: 77px;
  padding-right: ${metrics.baseMargin_1x}px;
  margin-right: ${props => props.isLastItem ? metrics.baseContainer + 4.5 : 0}px;
`;

export const Image = styled.ImageBackground`
  width: 100%;
  height: 100%;
  border-radius: 4px;

  justify-content: flex-end;

  overflow: hidden;
  resize-mode: cover;
`;

export const Wrap = styled.TouchableOpacity.attrs({
  activeOpacity: .8
})`
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;

  background-color: rgba(0,0,0, 0.4);
`

export const Legend = styled(fonts.size_5x)`
  font-weight: 600;
  line-height: 24px;
  letter-spacing: -0.5px;
  color: ${colors.white};
  text-align: center;
`;

