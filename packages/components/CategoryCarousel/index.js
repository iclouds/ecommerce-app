import React from 'react'
import { withNavigation } from 'react-navigation'

import { Container, Item, Legend, Image, Wrap } from './styles'

import { CategoryCarouselPlaceholder } from 'components'

function CategoryCarousel({ items, navigation, isLoaded = false, style }) {
  function isLastItem(items, index) {
    return items.length - 1 === index
  }

  function renderCarousel() {
    return (
      <Container horizontal style={style}>
        {items.map((item, index) => (
          <Item isLastItem={isLastItem(items, index)} key={item.id}>
            <Image source={{ uri: item.src ? item.src : '' }}>
              <Wrap
                key={item.id}
                onPress={() =>
                  navigation.navigate('Category', { category: item.name })
                }
              >
                <Legend>{item.name}</Legend>
              </Wrap>
            </Image>
          </Item>
        ))}
      </Container>
    )
  }

  function renderPlaceholder() {
    return <CategoryCarouselPlaceholder />
  }

  return isLoaded ? renderCarousel() : renderPlaceholder()
}

export default withNavigation(CategoryCarousel)
