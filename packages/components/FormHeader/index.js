import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Keyboard, Animated, Platform } from 'react-native'

import MenuItem from './MenuItem'

import { Container, Logo, Menu, AnimatedLogo } from './styles'

export default function Header({
  activeMenuItem,
  setActiveMenuItem,
  items,
  withoutLogo,
}) {
  const [logoSize] = useState({
    width: new Animated.Value(117),
    height: new Animated.Value(70),
  })

  useEffect(() => {
    const logoAnimationKeyboard = {
      hide: () => {
        Animated.parallel([
          Animated.timing(logoSize.width, {
            toValue: 117,
            duration: 300,
          }),
          Animated.timing(logoSize.height, {
            toValue: 70,
            duration: 300,
          }),
        ]).start()
      },
      show: () => {
        Animated.parallel([
          Animated.timing(logoSize.width, {
            toValue: 80,
            duration: 300,
          }),
          Animated.timing(logoSize.height, {
            toValue: 50,
            duration: 300,
          }),
        ]).start()
      },
    }

    if (Platform.OS === 'ios') {
      Keyboard.addListener('keyboardWillHide', logoAnimationKeyboard.hide)

      Keyboard.addListener('keyboardWillShow', logoAnimationKeyboard.show)
    }

    if (Platform.OS === 'android') {
      Keyboard.addListener('keyboardDidHide', logoAnimationKeyboard.hide)

      Keyboard.addListener('keyboardDidShow', logoAnimationKeyboard.show)
    }
  }, [])

  return (
    <Container>
      {!withoutLogo && (
        <AnimatedLogo style={{ ...logoSize }}>
          <Logo />
        </AnimatedLogo>
      )}

      <Menu>
        {items.map(item => (
          <MenuItem
            key={item.type}
            title={item.title}
            description={item.description}
            isActive={activeMenuItem === item.type}
            onPress={() => setActiveMenuItem(item.type)}
          />
        ))}
      </Menu>
    </Container>
  )
}

Header.defaultProps = {
  setActiveMenuItem: () => {},
  withoutLogo: false,
}

Header.propTypes = {
  setActiveMenuItem: PropTypes.func,
  activeMenuItem: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  withoutLogo: PropTypes.bool,
}
