import styled from 'styled-components/native';
import { Animated } from 'react-native';

import { fonts } from 'theme';

export const Container = styled.TouchableOpacity.attrs({
  activeOpacity: 0.85,
})`
  margin-right: 32px;
  margin-bottom: ${props => (props.haveDescription ? 26 : 0)}px;
`;

export const ItemText = styled(fonts.size_5x)`
  color: #333;
  opacity: ${props => (props.isActive ? 1 : 0.5)};
`;

export const Line = styled(Animated.View)`
  width: 90%;
  height: 3px;

  background: ${props => props.theme.secondary};
`;

export const Description = styled(fonts.size_1x)`
  position: absolute;
  left: 0;
  bottom: -26px;

  width: 100%;

  opacity: 0.7;
  color: #333;
`;
