import React, { useState, useEffect } from 'react';
import { Animated } from 'react-native';
import PropTypes from 'prop-types';

import {
  Container, ItemText, Line, Description,
} from './styles';

export default function MenuItem({
  title, isActive, onPress, description,
}) {
  const [lineWidth, _] = useState(new Animated.Value(0));
  const [elementWidth, setElementWidth] = useState(50);

  useEffect(() => {
    if (isActive) {
      return Animated.timing(lineWidth, {
        toValue: elementWidth - 10,
        duration: 500,
      }).start();
    }

    return Animated.timing(lineWidth, {
      toValue: 0,
      duration: 300,
    }).start();
  }, [isActive, elementWidth]);

  function isALongWord(width) {
    return width >= 150;
  }

  function getElementWidth(event) {
    const { width } = event.nativeEvent.layout;

    if (isALongWord(width)) return setElementWidth(width - 70);

    return setElementWidth(width);
  }

  return (
    <>
      <Container
        onPress={onPress}
        haveDescription={description.length > 0}
        onLayout={event => getElementWidth(event)}
      >
        <ItemText isActive={isActive}>{title}</ItemText>

        <Line style={{ width: lineWidth }} />
      </Container>

      <Description>{description}</Description>
    </>
  );
}

MenuItem.defaultProps = {
  isActive: false,
  description: '',
};

MenuItem.propTypes = {
  isActive: PropTypes.bool,
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  description: PropTypes.string,
};
