import React, { useState, useEffect } from 'react'
import { Dimensions } from 'react-native'
import Carousel from 'react-native-snap-carousel'

import { metrics } from 'theme'
import { ConvertFloatToMoney } from 'functions'

import {
  Container,
  ProductImage,
  CarouselWrap,
  Info,
  Description,
  Price,
  Dots,
  CustomPagination,
} from './styles'

export default function ProductCarousel({ product }) {
  const screenWidth = Dimensions.get('window').width
  const [carouselActiveItem, setCarouselActiveItem] = useState(0)

  const formatedPrice = ConvertFloatToMoney(product.price)

  function renderProduct({ item, index }) {
    return (
      <Container>
        <ProductImage source={{ uri: item.src }} />

        {product.images.length > 1 && (
          <Dots
            style={{
              transform: [{ translateX: -300 * 0.5 }],
            }}
          >
            <CustomPagination
              dotsLength={product.images.length}
              activeDotIndex={carouselActiveItem}
            />
          </Dots>
        )}
      </Container>
    )
  }

  return (
    <>
      <CarouselWrap>
        <Carousel
          data={product.images}
          sliderWidth={screenWidth}
          renderItem={renderProduct}
          removeClippedSubviews={false}
          itemWidth={screenWidth - metrics.baseContainer * 2}
          onBeforeSnapToItem={index => setCarouselActiveItem(index)}
        />
      </CarouselWrap>

      <Info>
        <Description>{product.name}</Description>

        <Price>{formatedPrice}</Price>
      </Info>
    </>
  )
}
