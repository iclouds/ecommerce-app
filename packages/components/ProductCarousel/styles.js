import styled from 'styled-components/native';
import { Pagination } from 'react-native-snap-carousel'

import { metrics, fonts, colors } from 'theme'

export const Container = styled.View`
  border-radius: ${metrics.baseRadius}px;
`;

export const ProductImage = styled.Image`
  width: 100%;
  height: ${metrics.screenHeight * 0.40}px;
  border-radius: ${metrics.baseRadius}px;
  resize-mode: contain;
`;

export const CarouselWrap = styled.View`
  margin-bottom: ${metrics.baseMargin_1x}px;
`;

export const Info = styled.View`
  padding: 0 ${metrics.baseContainer}px;
  margin-bottom: ${metrics.baseMargin_1x}px;
`;

export const Description = styled(fonts.size_3x)`
  line-height: 22px;
  color: ${colors.grey};
`;

export const Price = styled(fonts.size_5x)`
  font-weight: bold;
  color: ${props => props.theme.secondary};
`;

export const Dots = styled.View`
  position: absolute;
  left: 50%;
  bottom: -20px;

  width: 300px;
  border-radius: 16px;
`;

export const CustomPagination = styled(Pagination).attrs({
  dotStyle: {
    width: 8,
    height: 8,
    borderRadius: 5,
    backgroundColor: 'rgba(255, 255, 255, 0.92)'
  },
  inactiveDotOpacity: 0.4,
  inactiveDotScale: 0.8,
})`
  position: relative;
`
