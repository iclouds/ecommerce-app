import styled from 'styled-components/native'

import { metrics, colors, fonts } from 'theme'

export const Container = styled.TouchableOpacity.attrs({
  activeOpacity: 0.8,
})`
  width: 48.2%;
`

export const Photo = styled.Image`
  height: 200px;
  border-radius: 4px;
  margin-bottom: ${metrics.baseMargin_1x}px;

  resize-mode: cover;
`

export const PlaceholderImage = styled.View`
  height: 200px;
  border-radius: ${metrics.baseRadius}px;
  margin-bottom: ${metrics.baseMargin_1x}px;

  background-color: ${colors.light};
`

export const Info = styled.View``

export const Name = styled(fonts.size_1x)`
  line-height: 18px;
  letter-spacing: -0.29px;
  color: ${props => props.theme.primary};
`

export const WrapPrices = styled.View`
  flex-direction: row;
`

export const Price = styled(fonts.size_2x)`
  margin-top: 0;

  font-weight: 600;
  letter-spacing: -0.33px;
  color: ${props => props.theme.primary};
`

export const RegularPrice = styled(fonts.size_2x)`
  margin-top: 0;
  margin-right: 4px;

  font-weight: 400;
  color: ${colors.grey};
  letter-spacing: -0.33px;
  text-decoration: line-through;
`
