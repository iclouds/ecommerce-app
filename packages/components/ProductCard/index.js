import React from 'react'
import PropTypes from 'prop-types'
import { ConvertFloatToMoney } from 'functions'

import { Container, Photo, Info, Name, WrapPrices, Price, RegularPrice, PlaceholderImage } from './styles'

export default function ProductCard({ image, name, price, sale_price, onPress }) {
  function renderImage() {
    return (
      <>
        {image && <Photo source={{ uri: image }} />}

        {!image && <PlaceholderImage />}
      </>
    )
  }

  function renderPrice(){
    return sale_price ?
      <WrapPrices>
        <RegularPrice>{ConvertFloatToMoney(price)}</RegularPrice>

        <Price>
          {ConvertFloatToMoney(sale_price)}
        </Price>
      </WrapPrices> :
      <Price>{ConvertFloatToMoney(price)}</Price>
  }

  return (
    <Container onPress={onPress}>
      {renderImage()}

      <Info>
        <Name>{name}</Name>
        { renderPrice() }
      </Info>
    </Container>
  )
}

ProductCard.defaultProps = {
  image: '',
}

ProductCard.propTypes = {
  image: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  name: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  price: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  sale_price: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
}
