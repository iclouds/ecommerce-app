import styled from 'styled-components/native';

import { colors, metrics } from 'theme'

export const Container = styled.View`
  width: 100%;
  height: 1px;
  padding-horizontal: ${metrics.baseContainer}px;
`;

export const Line = styled.View`
  flex: 1;

  background-color: ${colors.greyest};
`
