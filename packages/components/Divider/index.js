import React from 'react'

import { Container, Line } from './styles'

export default function Divider({ styles }) {
  return (
    <Container style={styles}>
      <Line />
    </Container>
  )
}
