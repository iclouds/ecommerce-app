import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Picker, Platform, Text } from "react-native";

import {
  Label,
  ArrowDown,
  Container,
  PickerIOS,
  CustomPicker,
  CustomPickerIOS,
  PickerContainer
} from "./styles";

export default function Select({
  name,
  width,
  items,
  colors,
  setSelectItem,
  selectItemToCart
}) {
  const [selectedItem, setSelectedItem] = useState(items[0]);

  useEffect(() => {
    function getNumberOfInstallment(installment) {
      return installment.split("")[0];
    }

    if (setSelectItem) {
      setSelectItem(getNumberOfInstallment(selectedItem));
    }
  }, [selectedItem]);

  useEffect(() => {
    if (["Tamanho", "Numeração"].includes(name)) {
      selectItemToCart({ size: selectedItem });
    }

    if (name === "Cor") {
      selectItemToCart({ color: selectedItem });
    }
  }, [selectedItem]);

  return (
    <Container width={width}>
      <Label>{name}</Label>

      <PickerContainer>
        {Platform.OS === "android" ? (
          <CustomPicker
            mode="dropdown"
            selectedValue={selectedItem}
            onValueChange={value => setSelectedItem(value)}
          >
            {items &&
              items.map((item, index) => (
                <Picker.Item
                  key={index}
                  label={item}
                  value={item}
                  color={colors.primary}
                />
              ))}
          </CustomPicker>
        ) : (
          <CustomPickerIOS>
            <PickerIOS
              value={selectedItem}
              onValueChange={value => setSelectedItem(value)}
              options={items.map((item, index) => ({
                key: index,
                value: item,
                label: item
              }))}
            />

            <ArrowDown />
          </CustomPickerIOS>
        )}
      </PickerContainer>
    </Container>
  );
}

Select.defaultProps = {
  width: "100",
  selectItemToCart: () => {}
};

Select.propTypes = {
  width: PropTypes.string,
  selectItemToCart: PropTypes.func,
  name: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(PropTypes.string).isRequired
};
