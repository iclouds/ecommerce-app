import styled from 'styled-components/native'
import { Picker } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'
import SelectInput from 'react-native-select-input-ios'

import { colors, metrics, fonts } from 'theme'

export const Container = styled.View`
  width: ${props => props.width}%;
`

export const PickerContainer = styled.View`
  border-width: 0.5px;
  border-radius: ${metrics.baseRadius}px;
  border-color: ${props => props.theme.primary};
`

export const Label = styled(fonts.size_1x)`
  margin-bottom: 2px;

  color: ${colors.grey};
`

export const CustomPicker = styled.Picker`
  width: 100%;
  height: 40px;
`

export const CustomPickerIOS = styled.View`
  width: 100%;
  height: 40px;
  padding: 0 ${metrics.baseMargin_2x}px;

  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  z-index: 2;
`

export const PickerIOS = styled(SelectInput).attrs(props => ({
  labelStyle: {
    textAlign: 'left',
    color: props.theme.primary,
  },
}))`
  flex: 1;
`

export const ArrowDown = styled(Icon).attrs(props => ({
  name: 'chevron-down',
  size: 20,
  color: props.theme.primary,
}))`
  position: absolute;
  right: ${metrics.baseMargin_2x};
  z-index: -1;
`
