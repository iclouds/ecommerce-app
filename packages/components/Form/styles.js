import styled from 'styled-components/native'
import { Animated } from 'react-native'

import { fonts } from 'theme'

import LineInput from '../LineInput'
import ButtonComponents from '../Button'

export const Container = styled(Animated.View)``

export const Input = styled(LineInput)`
  margin-top: 47px;
`

export const PasswordReset = styled.TouchableOpacity.attrs({
  activeOpacity: 0.95,
})`
  margin-top: 16px;
`

export const Text = styled(fonts.size_2x)`
  color: #333;
  opacity: 0.5;
`

export const Button = styled(ButtonComponents)`
  width: 100%;
  height: 42px;

  background-color: ${props => props.theme.secondary};
`
