import React, { useRef, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Alert, Animated, Linking } from "react-native";

import { Container, Input, Button, PasswordReset, Text } from "./styles";

const INITIAL_BUTTON_POSITION = 400;

function Form({ inputs, type, schema, buttonText, navigation, style, submit }) {
  const refs = inputs.map(() => useRef(null));

  const [errors, setErrors] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [buttonMarginTop] = useState(
    new Animated.Value(INITIAL_BUTTON_POSITION)
  );

  function useOpacityAnimation() {
    const INITIAL_OPACITY = 0;

    const [opacityValue] = useState(new Animated.Value(INITIAL_OPACITY));

    return opacityValue;
  }

  const opacityValues = inputs.map(() => useOpacityAnimation());

  useEffect(() => {
    const DELAY = 150;

    Animated.stagger(
      DELAY,
      opacityValues.map(value =>
        Animated.timing(value, {
          toValue: 1,
          duration: 300,
          useNativeDriver: true
        })
      ),
      Animated.timing(buttonMarginTop, {
        toValue: 46,
        duration: 600,
        useNativeDriver: true
      }).start()
    ).start();
  }, []);

  async function handleSubmit() {
    setIsLoading(true);

    try {
      let formData = {};

      inputs.forEach(input => {
        formData = { ...formData, [input.type]: input.value };
      });

      await schema.validate(formData, {
        abortEarly: false,
        stripUnknown: true
      });

      submit(formData);

      setErrors([]);
    } catch (error) {
      setErrors(error.inner);
    } finally {
      setIsLoading(false);
    }
  }

  function handleError(field) {
    const fieldError = errors.filter(error => error.path === field);

    return fieldError[0];
  }

  function isLastInput(index) {
    return inputs.length === index + 1;
  }

  function handleNextInput(index) {
    if (isLastInput(index)) {
      return handleSubmit();
    }

    return refs[index + 1].current.focus();
  }

  function handleResetPassword () {
    Linking.canOpenURL('https://cutterjeans.com.br/recuperar-senha').then(supported => {
      if (supported) {
        Linking.openURL('https://cutterjeans.com.br/recuperar-senha');
      } else {
        console.log("Não houve como abrir a URL: " + 'https://cutterjeans.com.br/recuperar-senha');
      }
    });
  };

  return (
    <Container style={style}>
      {inputs.map((input, index) => (
        <Input
          {...input}
          ref={refs[index]}
          key={`input-${input.type}`}
          defaultValue={input.value}
          onSubmitEditing={() => handleNextInput(index)}
          style={{ ...input.style, opacity: opacityValues[index] }}
          error={handleError(input.type) && handleError(input.type).message}
        />
      ))}

      {type === "signin" && (
        <PasswordReset onPress={() => handleResetPassword()}>
          <Text>Recuperar senha</Text>
        </PasswordReset>
      )}

      <Animated.View style={{ transform: [{ translateY: buttonMarginTop }] }}>
        <Button
          text={buttonText}
          isLoading={isLoading}
          onPress={handleSubmit}
        />
      </Animated.View>
    </Container>
  );
}

Form.defaultProps = {
  schema: {}
};

Form.propTypes = {
  type: PropTypes.string,
  schema: PropTypes.shape(),
  navigation: PropTypes.shape(),
  buttonText: PropTypes.string.isRequired,
  inputs: PropTypes.arrayOf(PropTypes.shape()).isRequired
};

export default Form;
