import styled from "styled-components/native";

import { metrics, fonts, colors } from "theme";

export const Container = styled.TouchableOpacity`
  margin-bottom: ${metrics.baseMargin_3x}px;

  flex-direction: row;
  justify-content: space-between;
`;

export const Content = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9
})`
  flex-direction: row;
`;

export const Photo = styled.Image`
  width: 75px;
  height: 84px;
  border-radius: 4px;
  margin-right: ${metrics.baseMargin_1x}px;

  resize-mode: cover;
`;

export const Info = styled.View`
  width: ${metrics.screenWidth * 0.55};

  justify-content: ${props =>
    props.withTags ? "space-between" : "flex-start"};
`;

export const Attributes = styled.View``;

export const Name = styled(fonts.size_2x).attrs({
  numberOfLines: 1,
  ellipsizeMode: "tail"
})`
  margin-bottom: 4px;

  color: ${colors.grey};
`;

export const Tags = styled.View`
  flex-direction: row;
`;

export const Tag = styled(fonts.size_1x)`
  border-radius: ${metrics.baseRadius}px;
  padding: 1px ${metrics.baseMargin_1x}px;
  margin-right: ${metrics.baseMargin_1x}px;

  overflow: hidden;
  color: ${colors.white};
  background-color: ${props => props.color ? props.color : props.theme.secondaryVariant};
`

export const Values = styled.View`
  flex-direction: row;
`;

export const Value = styled(fonts.size_1x)`
  margin-right: ${metrics.baseMargin_1x}px;

  color: ${colors.grey};
`;

export const Remove = styled.View`
  align-items: flex-end;
  justify-content: center;
`;
