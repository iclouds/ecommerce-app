import React from "react";
import PropTypes from "prop-types";
import { withNavigation } from "react-navigation";

import { colors } from "theme";
import { ButtonIcon } from "components";
import { ConvertFloatToMoney } from "functions";

import {
  Tag,
  Info,
  Name,
  Tags,
  Photo,
  Value,
  Values,
  Remove,
  Content,
  Container,
  Attributes
} from "./styles";

function ProductCart({ product, removeItem, editProduct, navigation }) {
  const { id, name, images, size, color, amount, price, available } = product.item

  const formatedPrice = ConvertFloatToMoney(price);

  function handleRemoveItem() {
    removeItem();

    editProduct();
  }

  return (
    <Container>
      <Content
        onPress={() =>
          navigation.navigate("Product", {
            productId: id,
            productName: name
          })
        }
      >
        <Photo source={{ uri: images[0].src }} />

        <Info withTags={size !== "Padrão"}>
          <Attributes>
            <Name>{name}</Name>

            {size !== "Padrão" && (
              <Tags>
                {size && <Tag>{size}</Tag>}
                {!available && available !== undefined && <Tag color={colors.danger}>Quantidade indisponível</Tag>}
                {/* <Tag>{color}</Tag> */}
              </Tags>
            )}
          </Attributes>

          <Values>
            <Value>Qtd: {amount}</Value>

            <Value>{formatedPrice}</Value>
          </Values>
        </Info>
      </Content>

      <Remove>
        <ButtonIcon
          name="close"
          size={16}
          style={{
            width: 20,
            height: 20,
            borderRadius: 100,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: colors.danger
          }}
          onPress={() => handleRemoveItem()}
        />
      </Remove>
    </Container>
  );
}

ProductCart.propTypes = {
  removeItem: PropTypes.func.isRequired
};

export default withNavigation(ProductCart);
