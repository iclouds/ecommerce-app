import React from 'react'
import PropTypes from 'prop-types'

import { ButtonIcon } from 'components'

import { Container } from './styles'

export default function HeaderRight({
  dark,
  fontSize,
  iconNames,
  onPressIcons,
  cartItems,
}) {
  return (
    <Container>
      {iconNames &&
        iconNames.map((iconName, index) => (
          <ButtonIcon
            size={fontSize}
            key={index}
            name={iconName}
            cartItems={cartItems}
            onPress={onPressIcons[index]}
            style={{ marginRight: iconNames.length !== index + 1 ? 15 : 0 }}
          />
        ))}
    </Container>
  )
}

HeaderRight.defaultProps = {
  dark: true,
  fontSize: 22,
}

HeaderRight.proTypes = {
  dark: PropTypes.bool,
  iconsNames: PropTypes.arrayOf(PropTypes.string).isRequired,
  onPressIcons: PropTypes.arrayOf(PropTypes.string).isRequired,
  fontSize: PropTypes.number,
}
