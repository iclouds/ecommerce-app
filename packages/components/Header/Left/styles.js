import styled from 'styled-components/native'
import { Platform } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'

import { metrics } from 'theme'

export const Container = styled.View`
  margin-left: ${Platform.OS !== 'android' ? 20 : 0};
`

export const Left = styled.View`
  flex-direction: row;
`

export const Menu = styled.TouchableOpacity`
  margin-right: ${metrics.baseMargin_2x}px;
`

export const MenuIcon = styled(Icon).attrs(props => {
  return {
    size: 32,
    name: 'menu',
    color: props.dark ? props.theme.white : props.theme.dark,
  }
})``
