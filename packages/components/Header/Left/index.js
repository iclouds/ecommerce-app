import React from 'react'
import PropTypes from 'prop-types'
import { withNavigation, DrawerActions } from 'react-navigation'

import { ButtonIcon } from 'components'
import { Container, Left, Logo } from './styles'

function HeaderLeft({ dark, navigation, cartItems }) {
  return (
    <Container>
      <Left>
        <ButtonIcon
          name="menu"
          dark={dark}
          cartItems={cartItems}
          onPress={() => navigation.dispatch(DrawerActions.openDrawer())}
        />
      </Left>
    </Container>
  )
}

HeaderLeft.defaultProps = {
  dark: true,
}

HeaderLeft.propTypes = {
  dark: PropTypes.bool,
}

export default withNavigation(HeaderLeft)
