import React from 'react'
import PropTypes from 'prop-types'
import { TextInput } from 'react-native'

import {
  Label,
  Container,
  CustomInput,
  CustomDatepicker,
  CustomMaskedInput,
} from './styles'

export default function Input({
  inputRef,
  flex,
  date,
  name,
  type,
  style,
  label,
  value,
  onBlur,
  masked,
  options,
  editable,
  datepicker,
  placeholder,
  onChangeText,
  onDateChange,
  keyboardType,
  autoCapitalize,
  onSubmitEditing,
  autocompleteType,
}) {
  function renderNormalInput() {
    return (
      <CustomInput
        ref={inputRef}
        value={value}
        onBlur={onBlur}
        editable={editable}
        placeholder={placeholder}
        keyboardType={keyboardType}
        autoCapitalize={autoCapitalize}
        autocompleteType={autocompleteType}
        onSubmitEditing={onSubmitEditing}
        onChangeText={text => onChangeText(text, name)}
      />
    )
  }

  function renderDatePicker() {
    return (
      <CustomDatepicker
        ref={inputRef}
        date={date}
        value={value}
        onBlur={onBlur}
        cancelBtnText="Cancelar"
        confirmBtnText="Confirmar"
        onDateChange={date => onDateChange(date)}
      />
    )
  }

  function renderMaskedInput() {
    return (
      <CustomMaskedInput
        customTextInput={TextInput}
        customTextInputProps={{ ref: inputRef }}
        editable={editable}
        type={type}
        value={value}
        onBlur={onBlur}
        options={options}
        placeholder={placeholder}
        keyboardType={keyboardType}
        onSubmitEditing={onSubmitEditing}
        onChangeText={text => onChangeText(text, name)}
      />
    )
  }

  return (
    <Container flex={flex} style={style}>
      <Label>{label}:</Label>

      {datepicker && renderDatePicker()}

      {!datepicker && masked && renderMaskedInput()}

      {!datepicker && !masked && renderNormalInput()}
    </Container>
  )
}

Input.defaultProps = {
  flex: 1,
  style: {},
  masked: false,
  datepicker: false,
  date: '10-05-1990',
  keyboardType: 'default',
  autocompleteType: 'off',
  autoCapitalize: 'none',
  onChangeText: () => {},
  onSubmitEditing: () => {},
  inputRef: null,
  editable: true
}

Input.propTypes = {
  flex: PropTypes.number,
  masked: PropTypes.bool,
  style: PropTypes.shape(),
  datepicker: PropTypes.bool,
  onChangeText: PropTypes.func,
  onSubmitEditing: PropTypes.func,
  keyboardType: PropTypes.string,
  autoCapitalize: PropTypes.string,
  label: PropTypes.string.isRequired,
  autocompleteType: PropTypes.string,
  placeholder: PropTypes.string.isRequired,
}
