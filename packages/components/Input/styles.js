import styled, { css } from 'styled-components/native'
import DatePicker from 'react-native-datepicker'
import { TextInputMask } from 'react-native-masked-text'

import { FormatedDate } from 'functions'
import { colors, metrics, fonts } from 'theme'

function defaultInputProps(props) {
  return {
    returnKeyType: 'next',
    placeholderTextColor: colors.greyest,
    underlineColorAndroid: 'transparent',
    autoCompleteType: props.autocompleteType,
    autoCapitalize: props.type === 'name' ? 'words' : 'none',
  }
}

const inputStyles = css`
  width: 100%;
  height: 38px;
  padding: 0 12px;
  border-radius: ${metrics.baseRadius}px;

  background: ${colors.light};
`

export const Container = styled.View`
  flex: ${props => props.flex};
`

export const Label = styled(fonts.size_1x)`
  margin-left: 4px;

  font-size: 9px;
  color: ${colors.grey};
`

export const CustomInput = styled.TextInput.attrs(props => ({
  ...defaultInputProps(props),
}))`
  ${inputStyles}

  font-size: 15px;
  font-family: 'OpenSans-Regular';
  line-height: 22px;
  color: ${colors.greyDark};
`

export const CustomMaskedInput = styled(TextInputMask).attrs(props => ({
  ...defaultInputProps(props),
}))`
  ${inputStyles}

  font-size: 15px;
  font-family: 'OpenSans-Regular';
  line-height: 22px;
  color: ${colors.greyDark};
`

export const CustomDatepicker = styled(DatePicker).attrs(props => ({
  mode: 'date',
  format: 'DD-MM-YYYY',
  minDate: '01-01-1900',
  maxDate: FormatedDate(),
  placeholder: '00/00/00',
  showIcon: false,
  customStyles: {
    dateInput: {
      borderWidth: 0,
      borderColor: 'transparent',
      paddingHorizontal: 12,

      alignItems: 'flex-start',
    },
    dateText: {
      textAlign: 'left',
      color: colors.grey,
      fontSize: 15,
      fontFamily: 'OpenSans-Regular',
    },
    btnTextConfirm: { color: props.theme.secondary },
  },
}))`
  ${inputStyles}
  padding: 0;
`
