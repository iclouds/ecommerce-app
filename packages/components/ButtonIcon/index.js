import React from 'react'
import PropTypes from 'prop-types'

import { Container, CustomIcon, CartNumber, Number } from './styles'

export default function ButtonIcon({
  dark,
  name,
  size,
  style,
  onPress,
  cartItems,
}) {
  return (
    <Container onPress={onPress} style={style}>
      {name === 'shopping-cart' && (
        <CartNumber dark={dark}>
          <Number dark={dark}>{cartItems}</Number>
        </CartNumber>
      )}

      <CustomIcon dark={dark} name={name} size={size} />
    </Container>
  )
}

ButtonIcon.defaultProps = {
  dark: true,
  size: 28,
  style: {},
  cartItems: 0,
}

ButtonIcon.propTypes = {
  dark: PropTypes.bool,
  size: PropTypes.number,
  name: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  cartItems: PropTypes.number,
}
