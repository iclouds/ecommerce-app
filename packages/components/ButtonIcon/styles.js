import styled from 'styled-components/native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { fonts } from 'theme'

export const Container = styled.TouchableOpacity`
  position: relative;
`

export const CustomIcon = styled(Icon).attrs(props => ({
  size: 22,
  color: props.dark ? props.theme.white : props.theme.dark,
}))``

export const CartNumber = styled.View`
  position: absolute;
  top: -5px;
  right: -5px;
  z-index: 1;

  width: 17px;
  height: 17px;
  padding: 2px;
  border-radius: 100;
  border: 2px solid
    ${props => (props.dark ? props.theme.primary : props.theme.white)};

  align-items: center;
  justify-content: center;

  background: ${props => (props.dark ? props.theme.white : props.theme.dark)};
`

export const Number = styled(fonts.size_1x)`
  font-size: 7px;
  font-weight: bold;
  text-align: center;
  line-height: 10px;
  color: ${props => (props.dark ? props.theme.dark : props.theme.white)};
`
