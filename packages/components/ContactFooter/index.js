import React from "react";

import {
  Container,
  Name,
  Role,
  Text,
  Wrap,
  Arrow,
  Photo,
  PhotoShadow
} from "./styles";

export default function ContactFooter({ onPress, adminInfo }) {
  const { name, role, avatar } = adminInfo;

  return (
    <Container onPress={onPress}>
      <PhotoShadow>
        <Photo source={{ uri: avatar }} />
      </PhotoShadow>

      <Name>{name}</Name>

      <Role>{role}</Role>

      <Wrap>
        <Arrow />

        <Text>Olá, em que posso ajudar?</Text>
      </Wrap>
    </Container>
  );
}
