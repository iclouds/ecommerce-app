import styled from 'styled-components'

import { fonts } from 'theme'

export const Container = styled.TouchableOpacity.attrs({
  activeOpacity: 0.95,
})`
  position: relative;

  width: 100%;
  height: 165px;
  margin-top: 94px;

  align-items: center;

  background-color: ${props => props.theme.secondary};
`

export const Name = styled(fonts.size_1x)`
  margin-top: 42px;
  line-height: 14px;

  color: #efefef;
`

export const Role = styled(fonts.size_1x)`
  line-height: 14px;

  color: #efefef;
`

export const Wrap = styled.View`
  margin-top: 10px;

  align-items: center;
`

export const Text = styled(fonts.size_2x)`
  padding: 9.5px;
  border-radius: 4px;
  box-shadow: 0 3px 4px rgba(0, 0, 0, 0.15);
  line-height: 20px;

  background-color: #ededed;
  color: ${props => props.theme.secondary};
`

export const Arrow = styled.View`
  width: 0;
  height: 0;

  border-style: solid;
  border-left-color: transparent;
  border-left-width: 5.5px;
  border-right-color: transparent;
  border-right-width: 5.5px;
  border-bottom-color: #ededed;
  border-bottom-width: 5.5px;
`

export const PhotoShadow = styled.View.attrs({
  elevation: 3,
})`
  position: absolute;
  top: -46px;
  z-index: 999;

  width: 85px;
  height: 85px;
  border-radius: 40px;

  align-items: center;
  justify-content: center;

  background: transparent;
`

export const Photo = styled.Image`
  top: 1px;

  width: 80px;
  height: 80px;
  border-radius: 40px;

  background-color: #ffffff;
`
