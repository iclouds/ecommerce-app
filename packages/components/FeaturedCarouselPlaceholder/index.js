import React from 'react'
import Placeholder from 'rn-placeholder'

import { Container, PlaceholderImage } from './styles'

export default function FeaturedCarouselPlaceholder() {
  return (
    <Container>
      <Placeholder animation="fade">
        <PlaceholderImage />
      </Placeholder>
    </Container>
  )
}
