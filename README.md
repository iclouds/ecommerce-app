# iClouds Monorepo Boilerplate

Esse repositório trata-se de um boilerplate padrão para aplicações construidas em cima do conceito de _"monorepos"_.

## Requisitos

Para poder usufruir de todos os benefícios propostos neste _boilerplate_, então é necessário que as ferramentas listadas abaixo estejam instaladas globalmente na sua máquina.

- [yarn 1.x](https://yarnpkg.com/)
- [lerna 3.x](https://lerna.js.org/)

> Para evitar conflitos, tente sempre usar a mesma versões das dependencias em todos os _packages_

## Estrutura

```
├── packages
│   ├── apps
│   │   ├── <projeto1>
│   │   ├── <projeto2>
│   │   └── ...
│   ├── components
│   └── functions
├── package.json
└── lerna.json
```

Baseado na arquitetura padrão de _monorepo_, na raiz do repositório há um diretório `packages` onde ficam todo o código compartilhado entre si. Onde são divididos por uma pasta `apps` onde ficam os aplicativos que consumiram dos _components_ e funções compartilhadas. Ao lado dela há a pasta `components` onde ficarão todos os _components_ que podem ser reutilizados em uma ou mais aplicações, o mesmo se aplica a pasta `functions`, com a diferença em que será compartilhado somente funções puras.

Voltando a raiz do repositório, existe um arquivo `lerna.json` que é responsavel pela configuração do [lerna](https://lerna.js.org/) no projeto. A configuração atual define que o cliente de _npm_ que será usado será o [yarn](https://yarnpkg.com/) e que ele usará as _features_ do [_workspaces_ do _yarn_](https://yarnpkg.com/en/docs/workspaces).

## Comandos

Fazer o _link_ das dependencias para todos os projetos:

```shell
lerna bootstrap
```

Adicionar um _package_ como dependência no `package.json`:

> Certifique-se que você está no diretório do projeto em que deseja adicionar

```shell
lerna add <nomedopacote>
```

Apagar a `node_modules` de todos os _workspaces_:

```shell
lerna clean
```

## Configuração

### React Native

Para a configuração de projetos construídos com [_React Native_](https://facebook.github.io/react-native/), alguns passos devem ser efetuados para que o [_Metro bundler_](https://facebook.github.io/metro/) possa reconhecer a pasta `node_modules` na raiz do repositório. Estes passos precisam ser feitos até o _metro_ ter [suporte à importação via _symlinks_](https://github.com/facebook/metro/issues/1).

#### Criando um novo projeto

> Para iniciar um novo projeto certifique-se que você possui o [_react-native-cli_](https://www.npmjs.com/package/react-native-cli) instalado globalmente em sua máquina.

Usando o seu terminal, navegue até a pasta `packages/apps` no seu repositório:

```shell
  cd packages/apps
```

Inicie um novo projeto com o seguinte comando:

```shell
react-native init <nomedoprojeto>
```

#### Corrigindo o caminho da `node_modules`

Com o projeto criado, abra-o em seu editor favorito e use a função _"Search & Replace"_ e altere todas as ocorrencias de `node_modules/react-native/` para `../../../node_modules/react-native/`.

Altere o _script_ `start` dentro do seu `package.json` e acrescente o parametro `--projectRoot=../../../` logo após o comando `cli.js start`, como o exemplo abaixo:

```json
"start": "node ../../../node_modules/react-native/local-cli/cli.js start --projectRoot=../../../"
```

Altere o arquivo `metro.config.js` (caso exista) para corresponder com o seguinte:

```js
const path = require("path");

module.exports = {
  projectRoot: path.resolve(__dirname, "../../../"),
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: false,
      },
    }),
  },
};
```

#### Configurando no iOS

Algumas referências dentro do projeto devem ser alteradas através do XCode para corrigir o caminho do arquivo `index` inicial.

Com o repositório aberto em seu terminal, abra o arquivo do projeto com o XCode:

```shell
open packages/apps/<nomedoprojeto>/ios/<nomedoprojeto>.xcodeproj
```

Abra o arquivo `AppDelegate.m`, e localize a linha `jsBundleURLForBundleRoot:@"index"` e altere o caminho `index` para `packages/apps/<nomedoprojeto>/index`.

Ainda com o projeto aberto no Xcode, clique no nome do projeto à esquerda e vá até a aba `Build Phases > Bundle React Native code and Images` e altere o seu conteúdo para

```shell
export NODE_BINARY=node
export EXTRA_PACKAGER_ARGS="--entry-file packages/mobile/index.js"
../../../../node_modules/react-native/scripts/react-native-xcode.sh
```

Com tudo isso feito você poderá iniciar o projeto com o comando

```shell
yarn workspace <nomedoprojeto> start
```

E iniciar o emulador através do botão `Run` com um ícone de triangulo dentro do Xcode.

#### Configurando no Android

Para que o projeto funcione em dispositivos Android as seguintes configurações são necessárias.

Altere a seção _allprojects_ do arquivo `android/build.gradle` conforme o código a seguir:

```gradle
...
allprojects {
    repositories {
        mavenLocal()
        google()
        jcenter()
        maven {
            // All of React Native (JS, Obj-C sources, Android binaries) is installed from npm
            url "$rootDir/../../../../node_modules/react-native/android"
        }
    }
}
...
```

Para cada dependência nativa adicionada, deve ser incluído `../../../` ao caminho para a pasta _node_modules_ em `android/settings.gradle`, conforme o exemplo abaixo:

```gradle
...
include ':react-native-gesture-handler'
project(':react-native-gesture-handler').projectDir = new File(rootProject.projectDir, '../../../../node_modules/react-native-gesture-handler/android')

...
```

Altere o arquivo `android/app/build.gradle`, alterando o objeto `project.ext.react` conforme o exemplo e acrescentando `../../../` ao caminho para a pasta _node_modules_ em `apply from`:

```gradle
...
project.ext.react = [
    entryFile: "packages/apps/<nomedoprojeto>/index.js",
    root: "../../../../../"
]

apply from: "../../../../../node_modules/react-native/react.gradle"
...
```

Para finalizar, altere a função `getJSMainModuleName` do arquivo `MainApplication.java` do aplicativo para retornar o caminho do projeto

```java
@Override
protected String getJSMainModuleName() {
  return "packages/apps/<nomedoprojeto>/index";
}
```

## Publicação no iOS

### Bug do React Native v0.59.8 no iOS

Ao rodar o lerna bootstrap, uma condição não é atendida na no react-native instalado na node_modules, para corrigir você precisa entrar no seguinte arquivo:

```sh
[nome_do_projeto]/node_modules/react-native/React/Base/RTCModuleMethod.mm
```

Assim, adicione este comando:

```c++
  RCTReadString(input, "__attribute__((__unused__))");
```

Como da seguinte forma:

```c++
  static BOOL RCTParseUnused(const char **input)
  {
    return RCTReadString(input, "__unused") ||
          RCTReadString(input, "__attribute__((__unused__))") ||
          RCTReadString(input, "__attribute__((unused))");
  }
```

Obs. Esta instrução fica próxima a linha 90 e o mesmo é necessario parar efetuar a build e rodar o app no XCode para desenvolver no iOS na versão v0.59.8 do react-native.

## Publicação no Android

### Atualização do react-gesture-handler

Importante observar que caso atualize o gesture handler, será necessario instalar o react-native-webview com o seguinte comando:

```shell
  yarn add react-native-webview
```
